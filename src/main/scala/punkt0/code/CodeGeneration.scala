package punkt0
package code

import ast.Trees._
import analyzer.Symbols._
import analyzer.Types._
import cafebabe._
import AbstractByteCodes.{New => _, _}
import ByteCodes._

object CodeGeneration extends Phase[Program, Unit] {

  def typeToByteCode(tpe: Type): String = tpe match {
    case TBoolean   =>  "Z"
    case TInt       =>  "I"
    case TString    =>  "Ljava/lang/String;"
    case TUnit      =>  "V"
    case TClass(x)  =>  "L" + x.name + ";"
  }

  def returnStatement(tpe: Type): ByteCode = tpe match {
    case TBoolean | TInt  =>  IRETURN
    case TString          =>  ARETURN
    case TUnit            =>  RETURN
    case TClass(x)        =>  ARETURN
  }

  def loadHelper(classSymbol: ClassSymbol, varIdStore: Map[String, Int], ch: CodeHandler, tpe: Type, name: String): Unit = {
    if(varIdStore.contains(name)) load(ch, tpe, varIdStore(name))
    else  {
      ch << ALOAD_0
      ch << GetField(classSymbol.name, name, typeToByteCode(tpe))
    }
  }

  def load(ch: CodeHandler, tpe: Type, n: Int): Unit = tpe match {
    case TBoolean         =>  ch << ILoad(n)
    case TInt             =>  ch << ILoad(n)
    case TString          =>  ch << ALoad(n)
    case TUnit            =>  ch << ALoad(n)
    case TClass(x)        =>  ch << ALoad(n)
  }

  def store(ch: CodeHandler, tpe: Type, n: Int): Unit = tpe match {
    case TBoolean         =>  ch << IStore(n)
    case TInt             => ch << IStore(n)
    case TString          =>  ch << AStore(n)
    case TUnit            =>  ch << AStore(n)
    case TClass(x)        =>  ch << AStore(n)
  }

  def run(prog: Program)(ctx: Context): Unit = {
    val outDir = ctx.outDir.map(_.getPath+"/").getOrElse("./")
    val f = new java.io.File(outDir)
    if (!f.exists()) {
      f.mkdir()
    }
    val sourceName = ctx.file.get.getName

    def storeHelper(classSymbol: ClassSymbol, varIdStore: Map[String, Int], ch: CodeHandler, tpe: Type, name: String, expr: ExprTree): Unit = {
      if(varIdStore.contains(name)) {
        exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
        store(ch, tpe, varIdStore(name))
      }
      else  {
        ch << ALOAD_0
        exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
        ch << PutField(classSymbol.name, name, typeToByteCode(tpe))
      }
    }

    /** Writes the proper .class file in a given directory. An empty string for dir is equivalent to "./". */
    def generateClassFile(sourceName: String, ct: ClassDecl, dir: String): Unit = {
      // TODO: Create code handler, save to files ...
      val classFile = new ClassFile(ct.id.value, if(ct.parent.isDefined)  Some(ct.parent.get.value) else  Some("java/lang/Object"))
      classFile.setSourceFile(sourceName)

      // class variables
      val classConstructorCodeHandler = classFile.addConstructor().codeHandler
      if(ct.parent.isDefined) {
        classConstructorCodeHandler << ALOAD_0
        classConstructorCodeHandler << InvokeSpecial("L" + ct.parent.get.value + ";", ct.parent.get.value, "()V")
      }
      else {
        classConstructorCodeHandler << ALOAD_0
        classConstructorCodeHandler << InvokeSpecial("java/lang/Object", "<init>", "()V")
      }
      ct.vars.foreach{ cv  =>
        classFile.addField(typeToByteCode(cv.getSymbol.getType), cv.id.value)
        classConstructorCodeHandler << ALOAD_0
        exprTreeToByteCode(ct.getSymbol, classConstructorCodeHandler, cv.expr, Map[String, Int]())
        classConstructorCodeHandler << PutField(ct.id.value, cv.id.value, typeToByteCode(cv.id.getType))
      }
      classConstructorCodeHandler << RETURN
      classConstructorCodeHandler.freeze

      // class methods
      ct.methods.foreach { cm =>
        var argTypeString: String = ""
        cm.args.foreach { cma =>
          argTypeString += typeToByteCode(cma.id.getType)
        }
        val retType: String = typeToByteCode(cm.retType.getType)
        val methodHandler: MethodHandler = classFile.addMethod(retType, cm.id.value, argTypeString)
        generateMethodCode(methodHandler.codeHandler, cm)
      }

      classFile.writeToFile(outDir + ct.id.value + ".class")
    }

    // a mapping from variable symbols to positions in the local variables
    // of the stack frame
    def generateMethodCode(ch: CodeHandler, mt: MethodDecl): Unit = {
      val methSym = mt.getSymbol
      var varIdStore: Map[String, Int] = Map[String, Int]()

      var argCount: Int = 1
      mt.args.foreach { ma =>
        varIdStore += (ma.id.value -> argCount)
        argCount += 1
      }

      mt.vars.foreach { methodVariableDecl =>
        val freshVar: Int = ch.getFreshVar
        varIdStore += (methodVariableDecl.id.value -> freshVar)
        exprTreeToByteCode(methSym.classSymbol, ch, methodVariableDecl.expr, varIdStore)
        store(ch, methodVariableDecl.getSymbol.getType, freshVar)
      }
      mt.exprs.foreach(expr =>  exprTreeToByteCode(methSym.classSymbol, ch, expr, varIdStore))
      exprTreeToByteCode(methSym.classSymbol, ch, mt.retExpr, varIdStore)
      ch << returnStatement(mt.retType.getType)
      ch.freeze
    }

    def exprTreeToByteCode(classSymbol: ClassSymbol, ch: CodeHandler, exprTree: ExprTree, varIdStore: Map[String, Int]): Unit = exprTree match {
      case And(lhs, rhs) =>
        val lazyEval = ch.getFreshLabel("lazyEval")
        val skipLazyEval = ch.getFreshLabel("skipLazyEval")
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        ch << IfEq(lazyEval)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        ch << IfEq(lazyEval)
        ch << ICONST_1
        ch << Goto(skipLazyEval)
        ch << Label(lazyEval)
        ch << ICONST_0
        ch << Label(skipLazyEval)
      case Or(lhs, rhs) =>
        val lazyEval = ch.getFreshLabel("lazyEval")
        val skipLazyEval = ch.getFreshLabel("skipLazyEval")
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        ch << IfNe(lazyEval)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        ch << IfNe(lazyEval)
        ch << ICONST_0
        ch << Goto(skipLazyEval)
        ch << Label(lazyEval)
        ch << ICONST_1
        ch << Label(skipLazyEval)
      case Plus(lhs, rhs) =>
        if(lhs.getType == rhs.getType && lhs.getType == TInt) {
          exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
          exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
          ch << IADD
        }
        else {
          ch << DefaultNew("java/lang/StringBuilder")
          exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
          lhs.getType match {
            case TInt     =>  ch << InvokeVirtual("java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;")
            case TString  =>  ch << InvokeVirtual("java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;")
          }
          exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
          rhs.getType match {
            case TInt     =>  ch << InvokeVirtual("java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;")
            case TString  =>  ch << InvokeVirtual("java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;")
          }
          ch << InvokeVirtual("java/lang/StringBuilder", "toString", "()Ljava/lang/String;")
        }
      case Minus(lhs, rhs) =>
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        ch << ISUB
      case Times(lhs, rhs) =>
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        ch << IMUL
      case Div(lhs, rhs) =>
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        ch << IDIV
      case LessThan(lhs, rhs) =>
        val trueLabel = ch.getFreshLabel("true")
        val skipTrueLabel = ch.getFreshLabel("skipTrue")
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        ch << If_ICmpLt(trueLabel)
        ch << ICONST_0
        ch << Goto(skipTrueLabel)
        ch << Label(trueLabel)
        ch << ICONST_1
        ch << Label(skipTrueLabel)
      case Equals(lhs, rhs) =>
        val trueLabel = ch.getFreshLabel("true")
        val skipTrueLabel = ch.getFreshLabel("skipTrue")
        exprTreeToByteCode(classSymbol, ch, lhs, varIdStore)
        exprTreeToByteCode(classSymbol, ch, rhs, varIdStore)
        lhs.getType match {
          case TString | TClass(_) => ch << If_ACmpEq(trueLabel)
          case _  =>  ch << If_ICmpEq(trueLabel)
        }
        ch << ICONST_0
        ch << Goto(skipTrueLabel)
        ch << Label(trueLabel)
        ch << ICONST_1
        ch << Label(skipTrueLabel)
      case m: MethodCall  =>
        m.obj match {
          case id: Identifier =>  loadHelper(classSymbol, varIdStore, ch, m.obj.getType, id.value)
          case This() =>  load(ch, m.obj.getType, 0)
          case _ =>  exprTreeToByteCode(classSymbol, ch, m.obj, varIdStore)
        }
        var methodSignature: String = "("
        m.args.foreach{ arg =>
          exprTreeToByteCode(classSymbol, ch, arg, varIdStore)
          methodSignature += typeToByteCode(arg.getType)
        }
        methodSignature += ")" + typeToByteCode(m.getType)
        ch << InvokeVirtual(m.obj.getType.toString, m.meth.value, methodSignature)
      case IntLit(value) =>
        ch << Ldc(value)
      case StringLit(value)  =>
        ch << Ldc(value)
      case True() =>
        ch << ICONST_1
      case False()  =>
        ch << ICONST_0
      case id: Identifier  =>
        loadHelper(classSymbol, varIdStore, ch, id.getType, id.value)
      case This() =>
        ch << ALOAD_0
      case Null() =>
        ch << ACONST_NULL
      case New(tpe) =>
        ch << DefaultNew(tpe.value)
      case Not(expr)  =>
        val jumpToTrue = ch.getFreshLabel("true")
        val skipTrue = ch.getFreshLabel("skipTrue")
        exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
        ch << IfEq(jumpToTrue)
        ch << ICONST_0
        ch << Goto(skipTrue)
        ch << Label(jumpToTrue)
        ch << ICONST_1
        ch << Label(skipTrue)
      case Block(exprs) =>
        exprs.foreach(expr  => exprTreeToByteCode(classSymbol, ch, expr, varIdStore))
      case If(expr, thn, els) =>
        val falseLabel = ch.getFreshLabel("false")
        val skipFalse = ch.getFreshLabel("skipFalse")
        exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
        ch << IfEq(falseLabel)
        exprTreeToByteCode(classSymbol, ch, thn, varIdStore)
        ch << Goto(skipFalse)
        ch << Label(falseLabel)
        if(els.isDefined)
          exprTreeToByteCode(classSymbol, ch, els.get, varIdStore)
        ch << Label(skipFalse)
      case While(cond: ExprTree, body: ExprTree)  =>
        val skipBodyLabel = ch.getFreshLabel("skipWhileLabel")
        val repeat = ch.getFreshLabel("repeat")
        ch << Label(repeat)
        exprTreeToByteCode(classSymbol, ch, cond, varIdStore)
        ch << IfEq(skipBodyLabel)
        exprTreeToByteCode(classSymbol, ch, body, varIdStore)
        ch << Goto(repeat)
        ch << Label(skipBodyLabel)
      case Println(expr) =>
        ch << GetStatic("java/lang/System", "out", "Ljava/io/PrintStream;")
        expr.getType match {
          case TBoolean =>
            ch << DefaultNew("java/lang/StringBuilder")
            exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
            ch << InvokeVirtual("java/lang/StringBuilder", "append", "(Z)Ljava/lang/StringBuilder;")
            ch << InvokeVirtual("java/lang/StringBuilder", "toString", "()Ljava/lang/String;")
          case TInt     =>
            ch << DefaultNew("java/lang/StringBuilder")
            exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
            ch << InvokeVirtual("java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;")
            ch << InvokeVirtual("java/lang/StringBuilder", "toString", "()Ljava/lang/String;")
          case _  => exprTreeToByteCode(classSymbol, ch, expr, varIdStore)
        }
        ch << InvokeVirtual("java/io/PrintStream", "println", "(Ljava/lang/String;)V")
      case Assign(id, expr) =>
        storeHelper(classSymbol, varIdStore, ch, id.getType, id.value, expr)
      case LambdaCall(id: Identifier, args: List[ExprTree]) =>
    }

    // output code
    prog.classes foreach {
      ct => generateClassFile(sourceName, ct, outDir)
    }

    // main
    val main: MainDecl = prog.main
    val classFile = new ClassFile(main.obj.value, Some("java/lang/Object"))
//    val classFile = new ClassFile(main.obj.value, None)
    classFile.setSourceFile(sourceName)

    val mainConstructorCodeHandler: CodeHandler = classFile.addConstructor().codeHandler
    mainConstructorCodeHandler << ALOAD_0
    mainConstructorCodeHandler << InvokeSpecial("java/lang/Object", "<init>", "()V")
    mainConstructorCodeHandler << RETURN
    mainConstructorCodeHandler.freeze

    val mainSymbol: ClassSymbol = main.getSymbol
    var varIdStore: Map[String, Int] = Map[String, Int]()
    val ch: CodeHandler = classFile.addMainMethod.codeHandler
    main.vars.foreach { mainVariable =>
      val freshVar: Int = ch.getFreshVar
      varIdStore += (mainVariable.id.value -> freshVar)
      exprTreeToByteCode(mainSymbol, ch, mainVariable.expr, varIdStore)
      store(ch, mainVariable.getSymbol.getType, freshVar)
    }

    main.exprs.foreach(expr =>  exprTreeToByteCode(mainSymbol, ch, expr, varIdStore))
    ch << RETURN
    ch.freeze
    classFile.writeToFile(outDir + main.obj.value + ".class")
  }
}