package punkt0
package lexer

import java.io.File


object Lexer extends Phase[File, Iterator[Token]] {
  import Reporter._

  def run(f: File)(ctx: Context): Iterator[Token] = {
    val source = scala.io.Source.fromFile(f)
    var sendEOF = false
    var empty = false
    var first: Char = 'a'
    
    if(source.hasNext)  first = source.next()
    else  sendEOF = true

    val operators = List(':', ';', '.', ',', '!', '(', ')', '{', '}', '<', '+', '-', '*', '[', ']', '^')

    def eat(): Unit = {
      if(source.hasNext)
        first = source.next()
      else
        sendEOF = true
    }

    def parseIntegerLiteral(): Token = {
      var result = 0
      val pos = source.pos

      if(first.asDigit==0)
        eat()
      else while(!sendEOF && first.isDigit) {
        result = result * 10 + first.asDigit
        eat()
      }
      
      new INTLIT(result).setPos(f, pos)
    }

    def parseStringLiteral(): Token = {
      var result = ""
      val pos = source.pos

      if(source.hasNext) {
        // skipping first quote
        first = source.next()
        // constructing string until another quote or newline character
        while(source.hasNext && first!='"' && first!='\n') {
          result += first
          first = source.next()
        }
        if(first=='"') {
          // skipping final quote and special case when end of file
          // first is " now have to handle EOF and set first for handleNext
          if(source.hasNext)
            first = source.next()
          else
            sendEOF = true
          new STRLIT(result).setPos(f, pos)
        }
        else {
          // STRING " not closed and ended with newline character
          Reporter.error("Invalid string", NoPosition.setPos(f, pos))
          new Token(BAD).setPos(f, pos)
        }
      }
      else {
        // If the first if is false. no character after first "
        sendEOF = true
        Reporter.error("Invalid string", NoPosition.setPos(f, pos))
        new Token(BAD).setPos(f, pos)
      }
    }

    def parseIdentifierKeyword(): Token = {
      var result = ""
      val pos = source.pos
      
      while(!sendEOF && (first.isLetterOrDigit || first=='_')) {
        result += first
        eat()
      }
      result match {
        // Check if the String we constructed is a keyword
        // Default case triggers if not a keyword
        case "object" => new Token(OBJECT).setPos(f, pos)
        case "class" => new Token(CLASS).setPos(f, pos)
        case "def" => new Token(DEF).setPos(f, pos)
        case "override" => new Token(OVERRIDE).setPos(f, pos)
        case "var" => new Token(VAR).setPos(f, pos)
        case "Unit" => new Token(UNIT).setPos(f, pos)
        case "String" => new Token(STRING).setPos(f, pos)
        case "extends" => new Token(EXTENDS).setPos(f, pos)
        case "Int" => new Token(INT).setPos(f, pos)
        case "Boolean" => new Token(BOOLEAN).setPos(f, pos)
        case "while" => new Token(WHILE).setPos(f, pos)
        case "if" => new Token(IF).setPos(f, pos)
        case "else" => new Token(ELSE).setPos(f, pos)
        case "true" => new Token(TRUE).setPos(f, pos)
        case "false" => new Token(FALSE).setPos(f, pos)
        case "this" => new Token(THIS).setPos(f, pos)
        case "null" => new Token(NULL).setPos(f, pos)
        case "new" => new Token(NEW).setPos(f, pos)
        case "println" => new Token(PRINTLN).setPos(f, pos)
        case _ => new ID(result).setPos(f, pos)
      }
    }

    def parseDivideSkipComment(): Token = {
      val pos = source.pos
      
      if(source.hasNext) {
        // skipping first '/'
        first = source.next()
        if(first=='/') {
          // if second char is '/', means "//", Single line comment
          while(source.hasNext && first!='\n') {
            // skipping everything until EOF or '\n'
            first = source.next()
          }
          if(source.hasNext && first=='\n') {
            // if not EOF and reached '\n'
            first = source.next()
            handleNext()
          }
          else {
            // reached EOF
            sendEOF = true
            handleNext()
          }
        }
        else if(first=='*') {
          // if second char is '*', means "/*", Multi-line comment
          var breakFlag = false
          var error = false
          while(source.hasNext && !breakFlag) {
            first = source.next()
            // breaking conditions for comments
            // setting breakFlag true when we need to break out
            if(first=='*') {
              // if we see a '*', should check if comment is ending ('*/')
              if(source.hasNext) {
                first = source.next()
                if(first=='/') {
                  // if we find '/', valid comment
                  if(source.hasNext) {
                    first = source.next()
                    breakFlag = true
                  }
                  else {
                    sendEOF = true
                    breakFlag = true
                  }
                }
              }
              else {
                // error because we reached EOF and multi-line comments not closed
                sendEOF = true
                breakFlag = true
                error = true
              }
            }
          }
          if(source.hasNext && breakFlag) {
            if(error) {
              // if we flagged error
              Reporter.error("Multi-line comment not closed", NoPosition.setPos(f, pos))
              new Token(BAD).setPos(f, pos)
            }
            else
              // no error, Valid comment. return next token
              handleNext()
          }
          else if(!source.hasNext && !breakFlag) {
            // if EOF and multi-line comments not closed
            sendEOF = true
            Reporter.error("Multi-line comment not closed", NoPosition.setPos(f, pos))
            new Token(BAD).setPos(f, pos)
          }
          else {
            // if we closed the comment at EOF
            sendEOF = true
            if(error) {
              Reporter.error("Multi-line comment not closed", NoPosition.setPos(f, pos))
              new Token(BAD).setPos(f, pos)
            }
            else
              handleNext()
          }
        }
        else
          // "/*" or "//", so regular "/" and there is a next char
          new Token(DIV).setPos(f, pos)
      }
      else {
        // EOF after '/'
        sendEOF = true
        new Token(DIV).setPos(f, pos)
      }
    }

    def parseAssignmentEquals(): Token = {
      val pos = source.pos
      if(source.hasNext) {            // first char is = and there is a next char
        first = source.next()
        if(first=='=') {              // second char is =
          if(source.hasNext) {        // there is a char after ==
            first = source.next()
          }
          else
            // there is no char after ==
            sendEOF = true
          new Token(EQUALS).setPos(f, pos)
        }
        else if(first=='>') {
          if(source.hasNext) {        // there is a char after =>
            first = source.next()
          }
          else
          // there is no char after ==
            sendEOF = true
          new Token(LAMBDA).setPos(f, pos)
        }
        else
          // second char is not =
          new Token(EQSIGN).setPos(f, pos)
      }
      else{
        // first char is = and there is no next char
        sendEOF = true
        new Token(EQSIGN).setPos(f, pos)
      }
    }

    def parseAND(): Token = {
      val pos = source.pos
      if(source.hasNext) {
        first = source.next()        // skipping first &
        if(first=='&') {
          // if second char is '&'
          // after "&&" setting first for next token
          if(source.hasNext)
            first = source.next()
          else
            sendEOF = true
          new Token(AND).setPos(f, pos)
        }
        else {
          // second char is not '&'
          Reporter.error("Invalid operator: &", NoPosition.setPos(f, pos))
          new Token(BAD).setPos(f, pos)
        }
      }
      else {
        // case where EOF and ends with a single &
        sendEOF = true
        Reporter.error("Invalid operator: &", NoPosition.setPos(f, pos))
        new Token(BAD).setPos(f, pos)
      }
    }

    def parseOR(): Token = {
      val pos = source.pos
      if(source.hasNext) {
        first = source.next()        // skipping first |
        if(first=='|') {
          // if second char is '|'
          // after "||" setting first for next token
          if(source.hasNext)
            first = source.next()
          else
            sendEOF = true
          new Token(OR).setPos(f, pos)
        }
        else {
          // second char is not '|'
          Reporter.error("Invalid operator: |", NoPosition.setPos(f, pos))
          new Token(BAD).setPos(f, pos)
        }
      }
      else {
        // case where EOF and ends with a single |
        sendEOF = true
        Reporter.error("Invalid operator: |", NoPosition.setPos(f, pos))
        new Token(BAD).setPos(f, pos)
      }
    }

    def parseOperators(): Token = {
      val pos = source.pos
      val result = first
      // setting first else setting EOF
      if(source.hasNext)
        first = source.next()
      else
        sendEOF = true
      result match {
        case ':' => new Token(COLON).setPos(f, pos)
        case ';' => new Token(SEMICOLON).setPos(f, pos)
        case '.' => new Token(DOT).setPos(f, pos)
        case ',' => new Token(COMMA).setPos(f, pos)
        case '!' => new Token(BANG).setPos(f, pos)
        case '(' => new Token(LPAREN).setPos(f, pos)
        case ')' => new Token(RPAREN).setPos(f, pos)
        case '{' => new Token(LBRACE).setPos(f, pos)
        case '}' => new Token(RBRACE).setPos(f, pos)
        case '<' => new Token(LESSTHAN).setPos(f, pos)
        case '+' => new Token(PLUS).setPos(f, pos)
        case '-' => new Token(MINUS).setPos(f, pos)
        case '*' => new Token(TIMES).setPos(f, pos)
        case '[' => new Token(LSQUARE).setPos(f, pos)
        case ']' => new Token(RSQUARE).setPos(f, pos)
        case '^' => new Token(CAROT).setPos(f, pos)
        case _ =>
          Reporter.error("Invalid operator: " + result, NoPosition.setPos(f, pos))
          new Token(BAD).setPos(f, pos)
      }
    }

    def parseEOF(): Token = {
      val pos = source.pos
      sendEOF = false
      empty = true
      terminateIfErrors()
      
      new Token(EOF).setPos(f, pos)
    }

    def parseBAD(): Token = {
      val pos = source.pos
      
      // skipping BAD character and setting first else EOF
      if(source.hasNext)
        first = source.next()
      else
        sendEOF = true
      Reporter.error("Token error", NoPosition.setPos(f, pos))
      new Token(BAD).setPos(f, pos)
    }

    def handleWhitespace() = {
      while(source.hasNext && first.isWhitespace) {
        first = source.next()
      }
      if(!source.hasNext && first.isWhitespace)
        sendEOF = true
        
      handleNext()
    }

    def handleNext(): Token = {
      if(empty)
        throw new NoSuchElementException
      else if(sendEOF)
        parseEOF()
      else if(first.isLetter)
        parseIdentifierKeyword()
      else if(first.isDigit)
        parseIntegerLiteral()
      else if(first=='"')
        parseStringLiteral()
      else if(first=='/')
        parseDivideSkipComment()
      else if(first=='=')
        parseAssignmentEquals()
      else if(first=='&')
        parseAND()
      else if(first=='|')
        parseOR()
      else if(operators.contains(first))
        parseOperators()
      else if(first.isWhitespace)
        handleWhitespace()
      else
        parseBAD()
    }

    new Iterator[Token] {
      def hasNext: Boolean = !empty
      def next(): Token = handleNext()
    }
  }
}
