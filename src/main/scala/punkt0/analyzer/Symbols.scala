package punkt0
package analyzer

import Types._

import scala.collection.mutable.ListBuffer

object Symbols {

  trait Symbolic[S <: Symbol] {
    private var _sym: Option[S] = None

    def setSymbol(sym: S): this.type = {
      _sym = Some(sym)
      this
    }

    def getSymbol: S = _sym match {
      case Some(s) => s
      case None => sys.error("Accessing undefined symbol.")
    }
  }

  sealed abstract class Symbol extends Positioned with Typed {
    val id: Int = ID.next
    val name: String

    override def toString: String = name + "#" + id.toString
  }

  private object ID {
    private var c: Int = 0

    def next: Int = {
      val ret = c
      c = c + 1
      ret
    }
  }

  class GlobalScope {
    var mainClass: ClassSymbol = _
    var classes: Map[String, ClassSymbol] = Map[String, ClassSymbol]()

    def lookupClass(n: String): Option[ClassSymbol] = {
      if(classes.contains(n))
        Some(classes(n))
      else
        None
    }
  }

  class ClassSymbol(val name: String) extends Symbol {
    var parent: Option[ClassSymbol] = None
    var methods: Map[String, MethodSymbol] = Map[String, MethodSymbol]()
    var members: Map[String, VariableSymbol] = Map[String, VariableSymbol]()
    var lambdaMembers: Map[String, LambdaSymbol] = Map[String, LambdaSymbol]()

    def lookupMethod(n: String): Option[MethodSymbol] = {
      if(methods.contains(n))
        Some(methods(n))
      else if(parent.nonEmpty)
        parent.get.lookupMethod(n)
      else
        None
    }

    def lookupVar(n: String): Option[VariableSymbol] = {
      if(members.contains(n))
        Some(members(n))
      else if(parent.nonEmpty)
        parent.get.lookupVar(n)
      else
        None
    }
  }

  class MethodSymbol(val name: String, val classSymbol: ClassSymbol) extends Symbol {
    var params: Map[String, VariableSymbol] = Map[String, VariableSymbol]()
    var members: Map[String, VariableSymbol] = Map[String, VariableSymbol]()
    var lambdaParams: Map[String, LambdaSymbol] = Map[String, LambdaSymbol]()
    var lambdaMembers: Map[String, LambdaSymbol] = Map[String, LambdaSymbol]()
    var argList: List[VariableSymbol] = Nil
    var lambdaList: List[LambdaSymbol] = Nil
    var overridden: Option[MethodSymbol] = None

    def lookupVar(n: String): Option[VariableSymbol] = {
      if(members.contains(n))
        Some(members(n))
      else if(params.contains(n))
        Some(params(n))
      else
        classSymbol.lookupVar(n)
    }

    def lookupLambda(n: String): Option[LambdaSymbol] = {
      if(lambdaParams.contains(n))
        Some(lambdaParams(n))
      else if(lambdaMembers.contains(n))
        Some(lambdaMembers(n))
      else
        None
    }
  }

  class LambdaSymbol(val name: String, val symbol: Symbol) extends Symbol {
    var declared: Boolean = true
    var params: Map[String, VariableSymbol] = Map[String, VariableSymbol]()
    var argList: List[VariableSymbol] = Nil
    var argTypeList: List[Type] = Nil
    var retType: Type = _

    def lookupVar(n: String): Option[VariableSymbol] = {
      if(params.contains(n))
        Some(params(n))
      else
        None
    }
  }

  class VariableSymbol(val name: String) extends Symbol

}
