package punkt0
package analyzer

import ast.Trees._
import Symbols._
import punkt0.analyzer.Types._

import scala.collection.mutable.ListBuffer

object NameAnalysis extends Phase[Program, Program] {
  def run(prog: Program)(ctx: Context): Program = {

    // Step 1: Collect symbols in declarations
    // Step 2: Attach symbols to identifiers (except method calls) in method bodies
    // (Step 3:) Print tree with symbol ids for debugging

    // Make sure you check all constraints
    val globalScope: GlobalScope = new GlobalScope
    var classProcessed = scala.collection.mutable.Map[String, Boolean]()

    // hidden class for lambda handling
    val hiddenClassSymbol: ClassSymbol = new ClassSymbol("hiddenClass")
    globalScope.classes += ("hiddenClass" -> hiddenClassSymbol)
    var hiddenClassMethods = new ListBuffer[MethodDecl]
    var uniqueMethodName: Int = 0

    // setType(identifier, type)
    def setType(identifier: Identifier, typeTree: TypeTree) = {
      typeTree match {
        case BooleanType()  =>
          identifier.getSymbol.setType(TBoolean)
          typeTree.setType(TBoolean)
        case IntType()      =>
          identifier.getSymbol.setType(TInt)
          typeTree.setType(TInt)
        case StringType()   =>
          identifier.getSymbol.setType(TString)
          typeTree.setType(TString)
        case UnitType()     =>
          identifier.getSymbol.setType(TUnit)
          typeTree.setType(TUnit)
        case id: Identifier  =>
          identifier.getSymbol.setType(TClass(globalScope.lookupClass(id.value).get))
          id.setSymbol(globalScope.lookupClass(id.value).get)
          typeTree.setType(identifier.getType)
      }
    }

    def isConstantExprTree(exprTree: ExprTree): Boolean = exprTree match {
      case IntLit(_)    =>  true
      case StringLit(_) =>  true
      case True()       =>  true
      case False()      =>  true
      case Null()       =>  true
      case New(_)       =>  true
      case _            =>  false
    }

    def assignSymbol(expr: ExprTree, symbol: Symbol): Unit = {
      expr match {
        case t: This =>
          symbol match {
            case c: ClassSymbol     =>  t.setSymbol(c)
            case m: MethodSymbol    =>  t.setSymbol(m.classSymbol)
            case l: LambdaSymbol    =>  t.setSymbol(l.symbol match {
              case lc: ClassSymbol  =>  lc
              case lm: MethodSymbol =>  lm.classSymbol
                // maybe remove this and throw an error at the user
            })
          }
        case And(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Or(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Plus(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Minus(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Times(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Div(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case LessThan(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Equals(lhs: ExprTree, rhs: ExprTree) =>
          assignSymbol(lhs, symbol)
          assignSymbol(rhs, symbol)
        case Not(expr: ExprTree) =>
          assignSymbol(expr, symbol)
        case Block(exprs: List[ExprTree]) =>
          exprs.foreach(assignSymbol(_, symbol))
        case If(expr: ExprTree, thn: ExprTree, els: Option[ExprTree]) =>
          assignSymbol(expr, symbol)
          assignSymbol(thn, symbol)
          if(els.isDefined) assignSymbol(els.get, symbol)
        case While(cond: ExprTree, body: ExprTree) =>
          assignSymbol(cond, symbol)
          assignSymbol(body, symbol)
        case Println(expr: ExprTree) =>
          assignSymbol(expr, symbol)
        case Assign(id: Identifier, expr: ExprTree) =>
          assignSymbol(id, symbol)
          assignSymbol(expr, symbol)
        case MethodCall(obj: ExprTree, meth: Identifier, args: List[ExprTree], lambdas: List[ExprTree]) =>
          assignSymbol(obj, symbol)
          lambdas.foreach {
            case le: LambdaExpr =>  // TODO: handle anonymous functions
              /*
              Anonymous FUnction
              Create a new function
              give it a unique name
              save it to a unique class
               */
              val lambdaSymbol: LambdaSymbol = new LambdaSymbol(uniqueMethodName.toString, hiddenClassSymbol)
              le.setSymbol(lambdaSymbol)
              uniqueMethodName = uniqueMethodName + 1
              le.args.foreach { lea =>
                if(lambdaSymbol.params.contains(lea.id.value)) {
                  Reporter.error(
                    "multiple parameters with same name " + lea.id.value,
                    lea.asInstanceOf[Positioned]
                  )
                }
                else {
                  val variableSymbol: VariableSymbol = new VariableSymbol(lea.id.value).setPos(lea.asInstanceOf[Positioned])
                  lambdaSymbol.params += (lea.id.value -> variableSymbol)
                  lea.setSymbol(variableSymbol)
                  lea.id.setSymbol(variableSymbol)
                  setType(lea.id, lea.tpe)
                }
              }
              lambdaSymbol.argList = lambdaSymbol.params.values.toList
              assignSymbol(le.retExpr, lambdaSymbol)
            case id: Identifier => symbol match {
              case c: ClassSymbol =>
                if(c.lambdaMembers.contains(id.value)) id.setSymbol(c.lambdaMembers(id.value))
                else  Reporter.error("Lambda " + id.value + " is not declared", id.asInstanceOf[Positioned])
              case m: MethodSymbol =>
                if(m.lookupLambda(id.value).isDefined) id.setSymbol(m.lookupLambda(id.value).get)
                else  Reporter.error("Lambda " + id.value + " is not declared", id.asInstanceOf[Positioned])
            }
          }
          args.foreach(arg => assignSymbol(arg, symbol))
        case New(tpe: Identifier) =>
          if(globalScope.lookupClass(tpe.value).isDefined)  tpe.setSymbol(globalScope.lookupClass(tpe.value).get)
          else  Reporter.error("Type " + tpe.value + " is not defined", tpe.asInstanceOf[Positioned])
        case id: Identifier =>
          symbol match {
            case c: ClassSymbol   =>
              if(c.lookupVar(id.value).isDefined) id.setSymbol(c.lookupVar(id.value).get)
              else  Reporter.error("variable " + id.value + " is not declared", id.asInstanceOf[Positioned])
            case m: MethodSymbol  =>
              if(m.lookupVar(id.value).isDefined) id.setSymbol(m.lookupVar(id.value).get)
              else  Reporter.error("variable " + id.value + " is not declared", id.asInstanceOf[Positioned])
            case l: LambdaSymbol  =>
              if(l.lookupVar(id.value).isDefined) id.setSymbol(l.lookupVar(id.value).get)
              else  Reporter.error("variable " + id.value + " is not declared", id.asInstanceOf[Positioned])
          }
        case lc: LambdaCall =>
          lc.args.foreach(arg => assignSymbol(arg, symbol))
          symbol match {
            case c: ClassSymbol   =>
              if(c.lambdaMembers.contains(lc.id.value)) lc.id.setSymbol(c.lambdaMembers(lc.id.value))
              else  Reporter.error("Lambda " + lc.id.value + " is not declared", lc.asInstanceOf[Positioned])
            case m: MethodSymbol  =>
              if(m.lookupLambda(lc.id.value).isDefined) lc.id.setSymbol(m.lookupLambda(lc.id.value).get)
              else  Reporter.error("Lambda " + lc.id.value + " is not declared", lc.asInstanceOf[Positioned])
          }
        case _  =>
      }
    }

    def findClass(name: String): ClassDecl = {
      prog.classes.find(c => c.id.value == name).get
    }

    def findMethod(methodName: String, className: String): MethodDecl = {
      findClass(className).methods.find(m => m.id.value == methodName).get
    }

    def processClass(c: ClassDecl): Unit = {
      if(classProcessed(c.id.value)) {
        // nothing
      }
      else {
        if(c.parent.isDefined && !classProcessed(c.parent.get.value))
          processClass(findClass(c.parent.get.value))

        val classSymbol: ClassSymbol = globalScope.lookupClass(c.id.value).get

        // class members
        c.vars.foreach { cv =>
          if(classSymbol.lookupVar(cv.id.value).isDefined) {
            val firstDefined: VariableSymbol = classSymbol.lookupVar(cv.id.value).get
            Reporter.error(
              "variable " +
                cv.id.value +
                " in class " +
                c.id.value +
                " is already defined. First definition here: " +
                firstDefined.posString,
              cv.asInstanceOf[Positioned]
            )
          }
          else {
            val variableSymbol: VariableSymbol = new VariableSymbol(cv.id.value).setPos(cv.asInstanceOf[Positioned])
            classSymbol.members += (cv.id.value -> variableSymbol)
            cv.setSymbol(variableSymbol)
            cv.id.setSymbol(variableSymbol)
            assignSymbol(cv.expr, classSymbol)
            if(!isConstantExprTree(cv.expr))
              Reporter.error("variable initialized to non constant", cv.expr)
            setType(cv.id, cv.tpe)
          }
        }

        // class methods
        c.methods.foreach { cm =>
          if(classSymbol.lookupMethod(cm.id.value).isDefined && !cm.overrides) {
            val firstDefined: MethodSymbol = classSymbol.lookupMethod(cm.id.value).get
            Reporter.error(
              "method " +
                cm.id.value +
                " in class " +
                c.id.value +
                " is already defined. First definition here: " +
                firstDefined.posString,
              cm.asInstanceOf[Positioned]
            )
          }
          else {
            if(classSymbol.methods.contains(cm.id.value)) {
              val firstDefined: MethodSymbol = classSymbol.lookupMethod(cm.id.value).get
              Reporter.error(
                "method " +
                  cm.id.value +
                  " in class " +
                  c.id.value +
                  " is already defined. First definition here: " +
                  firstDefined.posString,
                cm.asInstanceOf[Positioned]
              )
            }
            else {
              val methodSymbol: MethodSymbol = new MethodSymbol(cm.id.value, classSymbol).setPos(cm.asInstanceOf[Positioned])
              classSymbol.methods += (cm.id.value -> methodSymbol)
              cm.setSymbol(methodSymbol)
              cm.id.setSymbol(methodSymbol)

              // method params
              cm.args.foreach { cma =>
                if(methodSymbol.params.contains(cma.id.value)) {
                  Reporter.error(
                    "multiple parameters with same name " + cma.id.value,
                    cma.asInstanceOf[Positioned]
                  )
                }
                else {
                  val variableSymbol: VariableSymbol = new VariableSymbol(cma.id.value).setPos(cma.asInstanceOf[Positioned])
                  methodSymbol.params += (cma.id.value -> variableSymbol)
                  cma.setSymbol(variableSymbol)
                  cma.id.setSymbol(variableSymbol)
                  setType(cma.id, cma.tpe)
                }
              }
              methodSymbol.argList = methodSymbol.params.values.toList

              // method lambdaArgs
              var tempLambdaList = new ListBuffer[LambdaSymbol]
              cm.lambdaArgs.foreach { cml =>
                if(methodSymbol.lookupVar(cml.id.value).isDefined || classSymbol.lookupMethod(cml.id.value).isDefined) {
                  Reporter.error(
                    "Another method/variable with same name already defined: " + cml.id.value,
                    cml.asInstanceOf[Positioned]
                  )
                }
                else {
                  val lambdaSymbol: LambdaSymbol = new LambdaSymbol(cml.id.value, methodSymbol).setPos(cml.asInstanceOf[Positioned])
                  lambdaSymbol.declared = false
                  methodSymbol.lambdaParams += (cml.id.value -> lambdaSymbol)
                  cml.setSymbol(lambdaSymbol)
                  cml.id.setSymbol(lambdaSymbol)
                  setType(cml.id, cml.retType)
                  var temp = new ListBuffer[Type]
                  cml.args.foreach {
                    case BooleanType() => temp += TBoolean
                    case UnitType() => temp += TUnit
                    case IntType() => temp += TInt
                    case StringType() => temp += TString
                    case id: Identifier => temp += TClass(globalScope.lookupClass(id.value).get)
                  }
                  lambdaSymbol.argTypeList = temp.toList
                  lambdaSymbol.retType = cml.retType match {
                    case BooleanType()  => TBoolean
                    case UnitType()     => TUnit
                    case IntType()      => TInt
                    case StringType()   => TString
                    case id: Identifier => TClass(globalScope.lookupClass(id.value).get)
                  }
                  tempLambdaList += lambdaSymbol
                }
              }
              methodSymbol.lambdaList = tempLambdaList.toList

              // method members
              cm.vars.foreach { cmv =>
                if(methodSymbol.members.contains(cmv.id.value)
                  || methodSymbol.params.contains(cmv.id.value)
                  || methodSymbol.lambdaParams.contains(cmv.id.value)) {
                  Reporter.error(
                    "Another method/variable with same name already defined: " + cmv.id.value + " is already defined",
                    cmv.asInstanceOf[Positioned]
                  )
                }
                else {
                  val variableSymbol: VariableSymbol = new VariableSymbol(cmv.id.value).setPos(cmv.asInstanceOf[Positioned])
                  methodSymbol.members += (cmv.id.value -> variableSymbol)
                  cmv.setSymbol(variableSymbol)
                  cmv.id.setSymbol(variableSymbol)
                  assignSymbol(cmv.expr, methodSymbol)
                  if(!isConstantExprTree(cmv.expr))
                    Reporter.error("variable initialized to non constant", cmv.expr)
                  setType(cmv.id, cmv.tpe)
                }
              }

              // method lambdas
              cm.lambdas.foreach { cml =>
                if(methodSymbol.lookupVar(cml.id.value).isDefined || classSymbol.lookupMethod(cml.id.value).isDefined || methodSymbol.lookupLambda(cml.id.value).isDefined) {
                  Reporter.error(
                    "Another method/variable with same name already defined: " + cml.id.value,
                    cml.asInstanceOf[Positioned]
                  )
                }
                else {
                  val lambdaSymbol: LambdaSymbol = new LambdaSymbol(cml.id.value, methodSymbol).setPos(cml.asInstanceOf[Positioned])
                  methodSymbol.lambdaMembers += (cml.id.value -> lambdaSymbol)
                  cml.setSymbol(lambdaSymbol)
                  cml.id.setSymbol(lambdaSymbol)
                  setType(cml.id, cml.retType)

                  cml.args.foreach { cmla =>
                    if(lambdaSymbol.params.contains(cmla.id.value)) {
                      Reporter.error(
                        "multiple parameters with same name " + cmla.id.value,
                        cmla.asInstanceOf[Positioned]
                      )
                    }
                    else {
                      val variableSymbol: VariableSymbol = new VariableSymbol(cmla.id.value).setPos(cmla.asInstanceOf[Positioned])
                      lambdaSymbol.params += (cmla.id.value -> variableSymbol)
                      cmla.setSymbol(variableSymbol)
                      cmla.id.setSymbol(variableSymbol)
                      setType(cmla.id, cmla.tpe)
                    }
                  }
                  lambdaSymbol.argList = lambdaSymbol.params.values.toList
                  lambdaSymbol.retType = cml.retType match {
                    case BooleanType()  => TBoolean
                    case UnitType()     => TUnit
                    case IntType()      => TInt
                    case StringType()   => TString
                    case id: Identifier => TClass(globalScope.lookupClass(id.value).get)
                  }

                  assignSymbol(cml.retExpr, lambdaSymbol)
                }
              }

              // setting overridden
              if(cm.overrides) {
                if(c.parent.isDefined) {
                  methodSymbol.overridden = c.parent.get.getSymbol.asInstanceOf[ClassSymbol].lookupMethod(cm.id.value)
                  if (methodSymbol.overridden.isDefined) {
                    if (methodSymbol.params.size != methodSymbol.overridden.get.params.size) {
                      Reporter.error("overridden method " +
                        methodSymbol.overridden.get.name +
                        " has different number of arguments",
                        cm.asInstanceOf[Positioned])
                    }
                    else {
                      val parentMethodSymbol: MethodSymbol = methodSymbol.overridden.get
                      if(!(methodSymbol.argList, parentMethodSymbol.argList).zipped.forall(_.getType == _.getType))
                        Reporter.error("overriding not possible, type mismatch with parameters", cm.asInstanceOf[Positioned])
                      if(cm.retType != findMethod(parentMethodSymbol.name, parentMethodSymbol.classSymbol.name).retType)
                        Reporter.error("type mismatch with overriding method's return type", cm.asInstanceOf[Positioned])
                    }
                  }
                  else Reporter.error("could not find method overridden by " + cm.id.value, cm.asInstanceOf[Positioned])
                }
                else Reporter.error("method overrides but there is no super class", cm.asInstanceOf[Positioned])
              }

              // classMethod.exprs
              cm.exprs.foreach(assignSymbol(_, methodSymbol))

              // classMethod.retExpr
              assignSymbol(cm.retExpr, methodSymbol)

              //classMethod.retType
              cm.retType match {
                case BooleanType()  =>  cm.retType.setType(TBoolean)
                case UnitType()     =>  cm.retType.setType(TUnit)
                case IntType()      =>  cm.retType.setType(TInt)
                case StringType()   =>  cm.retType.setType(TString)
                case id: Identifier =>  id.setSymbol(globalScope.lookupClass(id.value).get)
              }
            }
          }
        }
      }
      classProcessed(c.id.value) = true
    }

    // classes - phase one
    prog.classes.foreach { c =>
      if (globalScope.classes.contains(c.id.value)) {
        val firstDefined: ClassSymbol = globalScope.lookupClass(c.id.value).get
        Reporter.error(
          "class " +
            c.id.value +
            " is already defined. First definition here: " +
            firstDefined.posString,
          c.asInstanceOf[Positioned]
        )
      }
      else {
        val classSymbol: ClassSymbol = new ClassSymbol(c.id.value).setPos(c.asInstanceOf[Positioned])
        globalScope.classes += (c.id.value -> classSymbol)
        c.setSymbol(classSymbol)
        c.id.setSymbol(classSymbol)
        classProcessed += (c.id.value -> false)
        classSymbol.setType(TClass(classSymbol))
      }
    }

    // classes - parent
    prog.classes.foreach { c =>
      val classSymbol: ClassSymbol = globalScope.lookupClass(c.id.value).get

      if (c.parent.nonEmpty) {
        if (globalScope.classes.contains(c.parent.get.value)) {
          classSymbol.parent = globalScope.lookupClass(c.parent.get.value)
          c.parent.get.setSymbol(globalScope.lookupClass(c.parent.get.value).get)

          var classIter: ClassSymbol = globalScope.lookupClass(c.parent.get.value).get
          var loopInInheritenceFound: Boolean = false
          while(classIter.parent.isDefined) {
            if(classIter.name == classSymbol.name && !loopInInheritenceFound) {
              Reporter.error("loop in inheritance", c.asInstanceOf[Positioned])
              loopInInheritenceFound = true
            }
            classIter = classIter.parent.get
          }
        }
        else {
          Reporter.error(
            "parent class " +
              c.parent.get.value +
              " is not declared.",
            c.asInstanceOf[Positioned]
          )
        }
      }
    }

    Reporter.terminateIfErrors()

    prog.classes.foreach(processClass)

    if(globalScope.classes.contains(prog.main.obj.value)) {
      Reporter.error(
        "class exists with same name as object",
        prog.main.asInstanceOf[Positioned]
      )
    }
    else {
      val mainSymbol: ClassSymbol = new ClassSymbol(prog.main.obj.value).setPos(prog.main.asInstanceOf[Positioned])
      globalScope.mainClass = mainSymbol
      prog.main.setSymbol(mainSymbol)
      prog.main.obj.setSymbol(mainSymbol)

      // main members
      prog.main.vars.foreach { mv =>
        if(mainSymbol.members.contains(mv.id.value) || globalScope.lookupClass(mv.id.value).isDefined) {
          val firstDefined: VariableSymbol = mainSymbol.lookupVar(mv.id.value).get
          Reporter.error(
            "variable " +
              mv.id.value +
              " is already defined. First definition here: " +
              firstDefined.posString,
            mv.asInstanceOf[Positioned]
          )
        }
        else {
          val variableSymbol: VariableSymbol = new VariableSymbol(mv.id.value).setPos(mv.asInstanceOf[Positioned])
          mainSymbol.members += (mv.id.value -> variableSymbol)
          mv.setSymbol(variableSymbol)
          mv.id.setSymbol(variableSymbol)
          assignSymbol(mv.expr, mainSymbol)
          if(!isConstantExprTree(mv.expr))
            Reporter.error("variable initialized to non constant", mv.expr)
          setType(mv.id, mv.tpe)
        }
      }

      // main lambdas
      prog.main.lambdas.foreach { ml =>
        if(mainSymbol.lookupVar(ml.id.value).isDefined || globalScope.lookupClass(ml.id.value).isDefined)
          Reporter.error(
            "Invalid name: " + ml.id.value,
            ml.asInstanceOf[Positioned]
          )
        else {
          val lambdaSymbol: LambdaSymbol = new LambdaSymbol(ml.id.value, mainSymbol).setPos(ml.asInstanceOf[Positioned])
          mainSymbol.lambdaMembers += (ml.id.value -> lambdaSymbol)
          ml.setSymbol(lambdaSymbol)
          ml.id.setSymbol(lambdaSymbol)
          setType(ml.id, ml.retType)

          ml.args.foreach { mla =>
            if(lambdaSymbol.params.contains(mla.id.value)) {
              Reporter.error(
                "multiple parameters with same name " + mla.id.value,
                mla.asInstanceOf[Positioned]
              )
            }
            else {
              val variableSymbol: VariableSymbol = new VariableSymbol(mla.id.value).setPos(mla.asInstanceOf[Positioned])
              lambdaSymbol.params += (mla.id.value -> variableSymbol)
              mla.setSymbol(variableSymbol)
              mla.id.setSymbol(variableSymbol)
              setType(mla.id, mla.tpe)
            }
          }
          lambdaSymbol.argList = lambdaSymbol.params.values.toList
          lambdaSymbol.retType = ml.retType match {
            case BooleanType()  => TBoolean
            case UnitType()     => TUnit
            case IntType()      => TInt
            case StringType()   => TString
            case id: Identifier => TClass(globalScope.lookupClass(id.value).get)
          }

          assignSymbol(ml.retExpr, lambdaSymbol)
        }
      }

      // main.exprs
      prog.main.exprs.foreach(assignSymbol(_, mainSymbol))
    }
    Reporter.terminateIfErrors()

    prog
  }

}
