package punkt0
package analyzer

import ast.Trees._
import Symbols._
import Types._

object TypeChecking extends Phase[Program, Program] {

  /** TypeChecking does not produce a value, but has the side effect of
   * attaching types to trees and potentially outputting error messages. */
  def run(prog: Program)(ctx: Context): Program = {
    prog.classes.foreach { c =>
      // class variables
      c.vars.foreach { cv =>
        tcExpr(cv.expr, typeTreeToType(cv.tpe))
        tcExpr(cv.id, typeTreeToType(cv.tpe))
      }

      // class methods
      c.methods.foreach { cm =>
        // class method variable
        cm.vars.foreach { cmv =>
          tcExpr(cmv.expr, typeTreeToType(cmv.tpe))
          tcExpr(cmv.id, typeTreeToType(cmv.tpe))
        }

        // Lambda Declarations in Methods
        cm.lambdas.foreach { cml =>
          tcExpr(cml.retExpr, typeTreeToType(cml.retType))
        }

        // class method expressions
        cm.exprs.foreach(tcExpr(_))

        // class method return type
        cm.retType.setType(typeTreeToType(cm.retType))
        tcExpr(cm.retExpr, cm.retType.getType)
      }
    }

    prog.main.vars.foreach { mv =>
      tcExpr(mv.expr, typeTreeToType(mv.tpe))
      tcExpr(mv.id, typeTreeToType(mv.tpe))
    }
    prog.main.lambdas.foreach { ml =>
      tcExpr(ml.retExpr, typeTreeToType(ml.retType))
    }
    prog.main.exprs.foreach(tcExpr(_))

    // translates type
    def typeTreeToType(typeTree: TypeTree): Type = {
      typeTree match {
        case BooleanType()      =>  TBoolean
        case IntType()          =>  TInt
        case StringType()       =>  TString
        case UnitType()         =>  TUnit
        case id: Identifier     =>  TClass(findClass(id.value).getSymbol)
        case _  =>  sys.error("unknown error in typeTreeToType")
      }
    }

    // throws error if class does not
    def findClass(name: String): ClassDecl = {
      prog.classes.find(c => c.id.value == name).get
    }

    def tcExpr(expr: ExprTree, expected: Type*): Type = {
      val tpe: Type = expr match {
        case And(lhs: ExprTree, rhs: ExprTree) =>
          tcExpr(lhs, TBoolean)
          tcExpr(rhs, TBoolean)
          TBoolean
        case Or(lhs: ExprTree, rhs: ExprTree) =>
          tcExpr(lhs, TBoolean)
          tcExpr(rhs, TBoolean)
          TBoolean
        case Plus(lhs: ExprTree, rhs: ExprTree) =>
          val lhsType: Type = tcExpr(lhs, TString, TInt)
          val rhsType: Type = tcExpr(rhs, TString, TInt)
          if(lhsType==TInt && rhsType==TInt)
            TInt
          else
            TString
        case Minus(lhs: ExprTree, rhs: ExprTree) =>
          tcExpr(lhs, TInt)
          tcExpr(rhs, TInt)
          TInt
        case Times(lhs: ExprTree, rhs: ExprTree) =>
          tcExpr(lhs, TInt)
          tcExpr(rhs, TInt)
          TInt
        case Div(lhs: ExprTree, rhs: ExprTree) =>
          tcExpr(lhs, TInt)
          tcExpr(rhs, TInt)
          TInt
        case LessThan(lhs: ExprTree, rhs: ExprTree) =>
          tcExpr(lhs, TInt)
          tcExpr(rhs, TInt)
          TBoolean
        case Equals(lhs: ExprTree, rhs: ExprTree) =>
          val lhsType: Type = tcExpr(lhs)
          val rhsType: Type = tcExpr(rhs)
          if(lhsType==rhsType && (lhsType==TInt || lhsType==TString || lhsType==TUnit || lhsType==TBoolean))
            TBoolean
          else if(lhsType.isSubTypeOf(TAnyRef) && rhsType.isSubTypeOf(TAnyRef))
            TBoolean
          else
            TError
        case MethodCall(obj: ExprTree, meth: Identifier, args: List[ExprTree], lambdas: List[ExprTree]) =>
          tcExpr(obj) match {
            case TClass(classSymbol: ClassSymbol)  =>
              if(classSymbol.lookupMethod(meth.value).isDefined) {
                val methodSymbol: MethodSymbol = classSymbol.lookupMethod(meth.value).get
                if(findClass(methodSymbol.classSymbol.name).methods.exists(m => m.id.value == methodSymbol.name)) {
                  if(args.lengthCompare(methodSymbol.argList.size) == 0
                    && args.zip(methodSymbol.argList).forall {
                    case (mCallArg, mSymbolArg) => tcExpr(mCallArg, mSymbolArg.getType) == mSymbolArg.getType
                  }
                  ) {
                    if(lambdas.lengthCompare(methodSymbol.lambdaList.size) == 0
                      && lambdas.zip(methodSymbol.lambdaList).forall {
                        case (lambdaMethodCall, lambdaSymbol) => lambdaMethodCall match {
                          case lExpr: LambdaExpr  =>
                            (tcExpr(lExpr.retExpr, lambdaSymbol.retType) == lambdaSymbol.retType
                              && lExpr.args.zip(lambdaSymbol.argTypeList).forall {
                              case (argLMC, argTypeLS)  =>  typeTreeToType(argLMC.tpe) == argTypeLS
                            }
                              )
                          case lExpr: Identifier  =>  lExpr.getSymbol match {
                            case ls: LambdaSymbol =>
                              /*
                              the lambda is defined in the current scope
                               */
                              (lExpr.getSymbol.asInstanceOf[LambdaSymbol].retType == lambdaSymbol.retType
                              && lExpr.getSymbol.asInstanceOf[LambdaSymbol].argTypeList.zip(lambdaSymbol.argTypeList).forall {
                              case (argLMC, argTypeLS)  =>  argLMC == argTypeLS
                            }
                              )
                          }
                        }
                      }) {

                    }
                    else {
                      Reporter.error("lambda arguments mismatch in MethodDeclaration", meth)
                    }
                  }
                  else {
                    Reporter.error("arguments mismatch in MethodDeclaration", meth)
                  }
                  findClass(methodSymbol.classSymbol.name).methods.find(m => m.id.value == methodSymbol.name).get.retType match {
                    case BooleanType() => TBoolean
                    case IntType() => TInt
                    case StringType() => TString
                    case UnitType() => TUnit
                    case id: Identifier => TClass(classSymbol)
                    case _ => sys.error("TypeChecking:MethodCall")
                  }
                }
                else {
                  sys.error("TypeChecking:MethodCall")
                  TError
                }
              }
              else {
                Reporter.error("could not find method " + meth.value, meth)
                TError
              }
            case _ =>
              Reporter.error("could not find class of object", obj)
              TError
          }
        case lc: LambdaCall =>
          val lambdaSymbol: LambdaSymbol = lc.id.getSymbol.asInstanceOf[LambdaSymbol]
          if(lambdaSymbol.declared) {
            if(!(lc.args.lengthCompare(lambdaSymbol.argList.size) == 0
              && lc.args.zip(lambdaSymbol.argList).forall {
              case (mCallArg, mSymbolArg) => tcExpr(mCallArg, mSymbolArg.getType) == mSymbolArg.getType
            })) {
              Reporter.error("arguments mismatch in LambdaCall: " + lc.id.value, lc.id.asInstanceOf[Positioned])
            }
          }
          else {
            if(!(lc.args.lengthCompare(lambdaSymbol.argTypeList.size) == 0
              && lc.args.zip(lambdaSymbol.argTypeList).forall {
              case (mCallArg, mSymbolArgType) => tcExpr(mCallArg, mSymbolArgType) == mSymbolArgType
            })) {
              Reporter.error("arguments mismatch in LambdaCall: " + lc.id.value, lc.id.asInstanceOf[Positioned])
            }
          }
          lc.id.getType
        case IntLit(_) =>
          TInt
        case StringLit(_) =>
          TString
        case True() =>
          TBoolean
        case False() =>
          TBoolean
        case id: Identifier  =>
          id.getType
        case t: This =>
          t.getSymbol.getType
        case Null() =>
          TNull
        case New(tpe: Identifier) =>
          tpe.getType
        case Not(expr: ExprTree)  =>
          tcExpr(expr, TBoolean)
        case Block(exprs: List[ExprTree]) =>
          exprs.foreach(tcExpr(_))
          if(exprs.nonEmpty)  tcExpr(exprs.last)
          else  TUnit
        case If(expr: ExprTree, thn: ExprTree, els: Option[ExprTree]) =>
          tcExpr(expr, TBoolean)
          if(els.isDefined) {
            val thnType: Type = tcExpr(thn)
            val elsType: Type = tcExpr(els.get)
            if(thnType.isSubTypeOf(elsType))  elsType
            else if(elsType.isSubTypeOf(thnType)) thnType
            else {
              (thnType, elsType) match {
                case (TClass(x), TClass(y)) =>
                  var xAncestry: List[ClassSymbol] = Nil
                  var yAncestry: List[ClassSymbol] = Nil
                  var i: Option[ClassSymbol] = x.parent
                  while(i.isDefined) {
                    xAncestry = i.get :: xAncestry
                    i = i.get.parent
                  }
                  i = y.parent
                  while(i.isDefined) {
                    yAncestry = i.get :: yAncestry
                    i = i.get.parent
                  }
                  if(xAncestry.exists(xcs => yAncestry.contains(xcs)))
                    TClass(xAncestry.find(xcs => yAncestry.contains(xcs)).get)
                  else
                    TAnyRef
                case _  =>
                  TError
              }
            }
          }
          else  tcExpr(thn, TUnit)
        case While(cond: ExprTree, body: ExprTree) =>
          tcExpr(cond, TBoolean)
          tcExpr(body, TUnit)
          TUnit
        case Println(expr: ExprTree) =>
          tcExpr(expr, TString , TInt, TBoolean)
          TUnit
        case Assign(id: Identifier, expr: ExprTree) =>
          tcExpr(expr, id.getType)
          TUnit
        case _  =>
          sys.error("unknown error")
          TError
      }

      // Check result and return a valid type in case of error
      if (expected.isEmpty) {
        expr.setType(tpe)
        tpe
      } else if (!expected.exists(e => tpe.isSubTypeOf(e))) {
        Reporter.error("Type error: expected: " + expected.toList.mkString(" or ") + ", found: " + tpe, expr)
        expr.setType(expected.head)
        expected.head
      } else {
        expr.setType(tpe)
        tpe
      }
    }
    Reporter.terminateIfErrors()

    prog
  }

}
