package punkt0
package analyzer

import Symbols._

object Types {

  trait Typed {
    private var _tpe: Type = TNull

    def setType(tpe: Type): this.type = { _tpe = tpe; this }
    def getType: Type = _tpe
  }

  sealed abstract class Type {
    def isSubTypeOf(tpe: Type): Boolean
  }

  case object TError extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = false
    override def toString = "[error]"
  }

  case object TNull extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = true
    override def toString = "[null]"
  }

  case object TInt extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = tpe match {
      case TInt => true
      case _ => false
    }
    override def toString: String = "Int"
  }

  case object TBoolean extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = tpe match {
      case TBoolean =>  true
      case _  =>  false
    }
    override def toString: String = "Boolean"
  }

  case object TString extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = tpe match {
      case TString =>  true
      case _  =>  false
    }
    override def toString: String = "String"
  }

  case object TUnit extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = tpe match {
      case TUnit =>  true
      case _  =>  false
    }
    override def toString: String = "Unit"
  }

  case class TClass(classSymbol: ClassSymbol) extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = tpe match {
      case TAnyRef  =>  true
      case _  =>
        if(tpe == classSymbol.getType)  true
        else if(classSymbol.parent.isDefined)
          classSymbol.parent.get.getType.isSubTypeOf(tpe)
        else  false
    }
    override def toString: String = classSymbol.name
  }

  // special object to implement the fact that all objects are its subclasses
  case object TAnyRef extends Type {
    override def isSubTypeOf(tpe: Type): Boolean = false
    override def toString: String = "AnyRef"
  }
}
