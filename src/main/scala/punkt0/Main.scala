package punkt0

import java.io.File

import lexer._
import ast._
import analyzer._
import code._


object Main {

  def processOptions(args: Array[String]): Context = {
    var ctx = Context()

    def processOption(args: List[String]): Unit = args match {
      case "--help" :: args =>
        ctx = ctx.copy(doHelp = true)
        processOption(args)

      case "-d" :: out :: args =>
        ctx = ctx.copy(outDir = Some(new File(out)))
        processOption(args)

      case "--tokens" :: args =>
        ctx = ctx.copy(doTokens = true)
        processOption(args)

      case "--print" :: args =>
        ctx = ctx.copy(doPrintMain = true)
        processOption(args)

      case "--ast" :: args =>
        ctx = ctx.copy(doAST = true)
        processOption(args)

      case "--symid" :: args =>
        ctx = ctx.copy(doSymbolIds = true)
        processOption(args)

      case f :: args =>
        ctx = ctx.copy(file = Some(new File(f)))
        processOption(args)

      case List() =>
    }

    processOption(args.toList)

    if (ctx.doHelp) {
      displayHelp()
      sys.exit(0)
    }

    ctx
  }

  def displayHelp(): Unit = {
    println("Usage: <punkt0c> [options] <file>")
    println("Options include:")
    println(" --help        displays this help")
    println(" --tokens      prints the tokens")
    println(" --ast      		prints the AST")
    println(" --print    		pretty printer")
    println(" --symid      	pretty prints with symbol IDs")
    println(" -d <outdir>   generates class files in the specified directory")
  }

  def main(args: Array[String]): Unit = {
    var ctx = processOptions(args)

    // testConfig
    // ctx = ctx.copy(file = Some(new File("test.txt")))

    if(ctx.file.isEmpty) {
      println("Input files not given")
      sys.exit(1)
    }

    // lexer: Phase
    val lexerIterator: Iterator[Token] = Lexer.run(ctx.file.get)(ctx)

    if(ctx.doTokens)
      lexerIterator.foreach(f => println(f + "(" + f.line + ":" + f.column + ")"))
    else {
      // parser: Phase
      var prog: Trees.Program = Parser.run(lexerIterator)(ctx)

      if(ctx.doAST) {
        println(prog)
      }
      else if(ctx.doPrintMain) {
        println(Printer.apply(prog))
      }
      else {
        // nameAnalysis: Phase
        prog = NameAnalysis.run(prog)(ctx)

        if(ctx.doSymbolIds) {
          println(Printer.apply(prog, printID = true))
          sys.exit()
        }
        else {
          // typeChecking: Phase
          prog = TypeChecking.run(prog)(ctx)

          // codeGeneration: Phase
          CodeGeneration.run(prog)(ctx)
        }
      }
    }
  }
}
