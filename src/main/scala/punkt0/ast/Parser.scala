package punkt0
package ast

import Trees._
import lexer._

import scala.collection.mutable.ListBuffer

object Parser extends Phase[Iterator[Token], Program] {
  def run(tokens: Iterator[Token])(ctx: Context): Program = {
    import Reporter._
    /** Store the current token, as read from the lexer. */
    var currentToken: Token = new Token(BAD)

    def readToken(): Unit = {
      if (tokens.hasNext) {
        // uses nextToken from the Lexer trait
        currentToken = tokens.next

        // skips bad tokens
        while (currentToken.kind == BAD) {
          currentToken = tokens.next
        }
      }
    }

    /** ''Eats'' the expected token, or terminates with an error. */
    def eat(kind: TokenKind): Unit = {
      if (currentToken.kind == kind) {
        readToken()
      } else {
        expected(kind)
      }
    }

    /** Complains that what was found was not expected. The method accepts arbitrarily many arguments of type TokenKind */
    def expected(kind: TokenKind, more: TokenKind*): Nothing = {
      fatal("expected: " + (kind::more.toList).mkString(" or ") + ", found: " + currentToken, currentToken)
    }

    def eatLiteral(): ExprTree = {
      val token = currentToken
      val pos = currentToken.asInstanceOf[Positioned]

      currentToken.kind match {
        case INTLITKIND =>
          eat(INTLITKIND)
          IntLit(token.asInstanceOf[INTLIT].value).setPos(pos)
        case STRLITKIND =>
          eat(STRLITKIND)
          StringLit(token.asInstanceOf[STRLIT].value).setPos(pos)
        case IDKIND =>
          eat(IDKIND)
          if(currentToken.kind==LPAREN) {
            var repeatFlag: Boolean = false
            var args = new ListBuffer[ExprTree]
            eat(LPAREN)
            while(currentToken.kind!=RPAREN) {
              if(repeatFlag)  eat(COMMA)
              repeatFlag = true
              args += parseExpression()
            }
            eat(RPAREN)

            LambdaCall(Identifier(token.asInstanceOf[ID].value), args.toList)
          }
          else  Identifier(token.asInstanceOf[ID].value).setPos(pos)
        case TRUE =>
          eat(TRUE)
          True().setPos(pos)
        case FALSE =>
          eat(FALSE)
          False().setPos(pos)
        case THIS =>
          eat(THIS)
          This().setPos(pos)
        case NULL =>
          eat(NULL)
          Null().setPos(pos)
        case _ =>
          Reporter.error("Expected: Literal, Found:" + currentToken, pos)
          sys.exit(1)
      }
    }

    def parseLambda(): LambdaExpr = {
      val pos = currentToken.asInstanceOf[Positioned]
      var args = new ListBuffer[Formal]
      var repeatFlag: Boolean = false
      eat(LSQUARE)
      while(currentToken.kind!=RSQUARE) {
        if(repeatFlag)  eat(COMMA)
        repeatFlag = true
        var argID: Identifier = null
        currentToken.kind match {
          case IDKIND =>
            argID = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
            eat(IDKIND)
          case _  =>
            Reporter.error("Error in MethodCall", pos)
        }
        eat(COLON)
        args += Formal(parseType(), argID)
      }
      eat(RSQUARE)
      eat(LAMBDA)

      LambdaExpr(args.toList, parseExpression()).setPos(pos)
    }

    def eatNamedLambda(): ExprTree = {
      val pos = currentToken.asInstanceOf[Positioned]
      var meth: Identifier = null
      eat(CAROT)
      currentToken.kind match {
        case IDKIND =>
          meth = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in MethodCall", pos)
      }
      meth
    }

    def eatMethodCall(prev: ExprTree): ExprTree = {
      val pos = prev.asInstanceOf[Positioned]
      var meth: Identifier = null
      eat(DOT)
      currentToken.kind match {
        case IDKIND =>
          meth = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in MethodCall", pos)
      }
      eat(LPAREN)
      var args = new ListBuffer[ExprTree]
      var lambdas = new ListBuffer[ExprTree]
      var repeatFlag: Boolean = false
      while(currentToken.kind != RPAREN) {
        if(repeatFlag)  eat(COMMA)
        repeatFlag = true
        if(currentToken.kind == LSQUARE)  lambdas += parseLambda
        else if(currentToken.kind == CAROT) lambdas += eatNamedLambda
        else  args += parseExpression
      }
      eat(RPAREN)

      MethodCall(prev, meth, args.toList, lambdas.toList).setPos(pos)
    }


    def restOfExpression(): ExprTree = {
      var expr: ExprTree = null

      if(currentToken.kind == LPAREN) {
        eat(LPAREN)
        expr = parseExpression()
        eat(RPAREN)
      }
      else if(currentToken.kind == NEW) {
        val pos: Positioned = currentToken.asInstanceOf[Positioned]
        eat(NEW)
        val tpe:Identifier = Identifier(currentToken.asInstanceOf[ID].value)
        tpe.setPos(currentToken.asInstanceOf[Positioned])
        expr = New(tpe).setPos(pos)
        eat(IDKIND)
        eat(LPAREN)
        eat(RPAREN)
      }
      else if(currentToken.kind == WHILE) {
        val pos: Positioned = currentToken.asInstanceOf[Positioned]
        eat(WHILE)
        eat(LPAREN)
        val condition = parseExpression()
        eat(RPAREN)
        expr = While(condition, parseExpression()).setPos(pos)
      }
      else if(currentToken.kind == IF) {
        val pos: Positioned = currentToken.asInstanceOf[Positioned]
        eat(IF)
        eat(LPAREN)
        val condition = parseExpression()
        eat(RPAREN)
        val ifBlock = parseExpression()
        if(currentToken.kind == ELSE) {
          eat(ELSE)
          expr = If(condition, ifBlock, Some(parseExpression())).setPos(pos)
        }
        else
          expr = If(condition, ifBlock, None).setPos(pos)
      }
      else if(currentToken.kind == PRINTLN) {
        val pos: Positioned = currentToken.asInstanceOf[Positioned]
        eat(PRINTLN)
        eat(LPAREN)
        expr = Println(parseExpression()).setPos(pos)
        eat(RPAREN)
      }
      else if(currentToken.kind == LBRACE) {
        val pos: Positioned = currentToken.asInstanceOf[Positioned]
        var exprs = new ListBuffer[ExprTree]
        eat(LBRACE)
        if(currentToken.kind != RBRACE)
          exprs += parseExpression
        while(currentToken.kind != RBRACE) {
          eat(SEMICOLON)
          exprs += parseExpression
        }
        eat(RBRACE)
        expr = Block(exprs.toList).setPos(pos)
      }
      else { // is a literal
        expr = eatLiteral()
      }

      if(currentToken.kind == EQSIGN) {
        val pos = expr.asInstanceOf[Positioned]
        eat(EQSIGN)
        expr match {
          case id: Identifier => expr = Assign(id, parseExpression()).setPos(pos)
          case _ => Reporter.error("Error in assignment", pos)
        }
      }
      
      expr
    }

    def eatFactor(): ExprTree = {
      var expr: ExprTree = restOfExpression()
      while(currentToken.kind == DOT) {
        currentToken.kind match {
          case DOT  =>
            expr = eatMethodCall(expr)
        }
      }

      expr
    }

    def eatNegation(): ExprTree = {
      var expr: ExprTree = null
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      if(currentToken.kind == BANG) {
        eat(BANG)
        expr = Not(eatFactor()).setPos(pos)
      }
      else {
        expr = eatFactor()
      }

      expr
    }

    def eatTerm(): ExprTree = {
      var expr: ExprTree = eatNegation()
      val pos = expr.asInstanceOf[Positioned]
      while(currentToken.kind == TIMES || currentToken.kind == DIV) {
        currentToken.kind match {
          case TIMES  =>
            eat(TIMES)
            expr = Times(expr, eatNegation()).setPos(pos)
          case DIV   =>
            eat(DIV)
            expr = Div(expr, eatNegation()).setPos(pos)
        }
      }

      expr
    }

    def eatCompareTerm(): ExprTree = {
      var expr: ExprTree = eatTerm()
      val pos = expr.asInstanceOf[Positioned]
      while(currentToken.kind == PLUS || currentToken.kind == MINUS) {
        currentToken.kind match {
          case PLUS  =>
            eat(PLUS)
            expr = Plus(expr, eatTerm()).setPos(pos)
          case MINUS   =>
            eat(MINUS)
            expr = Minus(expr, eatTerm()).setPos(pos)
        }
      }

      expr
    }

    def eatConditionTerm(): ExprTree = {
      var expr: ExprTree = eatCompareTerm()
      val pos = expr.asInstanceOf[Positioned]
      while(currentToken.kind == LESSTHAN || currentToken.kind == EQUALS) {
        currentToken.kind match {
          case LESSTHAN  =>
            eat(LESSTHAN)
            expr = LessThan(expr, eatCompareTerm()).setPos(pos)
          case EQUALS   =>
            eat(EQUALS)
            expr = Equals(expr, eatCompareTerm()).setPos(pos)
        }
      }

      expr
    }

    def eatAnd(): ExprTree = {
      var expr: ExprTree = eatConditionTerm()
      val pos = expr.asInstanceOf[Positioned]
      while(currentToken.kind == AND) {
        eat(AND)
        expr = And(expr, eatConditionTerm()).setPos(pos)
      }

      expr
    }

    def eatOr(): ExprTree = {
      var expr: ExprTree = eatAnd()
      val pos = expr.asInstanceOf[Positioned]
      while(currentToken.kind == OR) {
        eat(OR)
        expr = Or(expr, eatAnd()).setPos(pos)
      }

      expr
    }

    def parseExpression(): ExprTree = {
      eatOr()
    }

    def parseType(): TypeTree = {
      var tpe: TypeTree = null
      currentToken.kind match {
        case BOOLEAN =>
          tpe = BooleanType().setPos(currentToken.asInstanceOf[Positioned])
          eat(BOOLEAN)
        case INT =>
          tpe = IntType().setPos(currentToken.asInstanceOf[Positioned])
          eat(INT)
        case STRING =>
          tpe = StringType().setPos(currentToken.asInstanceOf[Positioned])
          eat(STRING)
        case UNIT =>
          tpe = UnitType().setPos(currentToken.asInstanceOf[Positioned])
          eat(UNIT)
        case IDKIND =>
          tpe = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
      }

      tpe
    }

    def parseMethodDecl(): MethodDecl = {
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      var overrides: Boolean = false
      var retType: TypeTree = null
      var id: Identifier = null
      var argsList = new ListBuffer[Formal]
      var funcArgsList = new ListBuffer[LambdaInit]
      var varList = new ListBuffer[VarDecl]
      var lambdaList = new ListBuffer[LambdaDecl]
      var exprList = new ListBuffer[ExprTree]
      var retExpr: ExprTree = null

      if(currentToken.kind == OVERRIDE) {
        overrides = true
        eat(OVERRIDE)
      }
      eat(DEF)
      currentToken.kind match {
        case IDKIND =>
          id = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in MethodDecl", pos)
      }
      eat(LPAREN)
      var repeatFlag: Boolean = false
      var noMoreRegularArgs: Boolean = false
      while(currentToken.kind != RPAREN) {
        if(repeatFlag)  eat(COMMA)
        repeatFlag = true
        var argID: Identifier = null
        currentToken.kind match {
          case IDKIND =>
            argID = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
            eat(IDKIND)
          case _  =>
            Reporter.error("Error in MethodDecl", pos)
        }
        eat(COLON)
        if(currentToken.kind == LPAREN) noMoreRegularArgs = true
        if(noMoreRegularArgs) {
          var typeList = new ListBuffer[TypeTree]
          eat(LPAREN)
          var flagMultiple: Boolean = false
          while(currentToken.kind!=RPAREN) {
            if(flagMultiple)  eat(COMMA)
            flagMultiple = true
            typeList += parseType()
          }
          eat(RPAREN)
          eat(LAMBDA)
          funcArgsList += LambdaInit(parseType(), typeList.toList, argID)
        }
        else {
          argsList += Formal(parseType(), argID).setPos(argID.asInstanceOf[Positioned])
        }
      }
      eat(RPAREN)
      eat(COLON)
      currentToken.kind match {
        case BOOLEAN  =>
          retType = BooleanType()
          eat(BOOLEAN)
        case INT  =>
          retType = IntType()
          eat(INT)
        case STRING =>
          retType = StringType()
          eat(STRING)
        case UNIT =>
          retType = UnitType()
          eat(UNIT)
        case IDKIND =>
          retType = Identifier(currentToken.asInstanceOf[ID].value)
          eat(IDKIND)
      }
      eat(EQSIGN)
      eat(LBRACE)
      while(currentToken.kind == VAR) varList += parseVarDecl
      while(currentToken.kind == DEF) lambdaList += parseLambdaDecl
      exprList += parseExpression
      while(currentToken.kind == SEMICOLON) {
        eat(SEMICOLON)
        exprList += parseExpression
      }
      eat(RBRACE)
      retExpr = exprList.toList.last

      MethodDecl(overrides, retType, id, argsList.toList, funcArgsList.toList, varList.toList, lambdaList.toList, exprList.dropRight(1).toList, retExpr).setPos(pos)
    }

    def parseVarDecl(): VarDecl = {
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      var tpe: TypeTree = null
      var id: Identifier = null
      var expr: ExprTree = null

      eat(VAR)
      currentToken.kind match {
        case IDKIND =>
          id = Identifier(currentToken.asInstanceOf[ID].value).setPos(pos)
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in VarDecl", pos)
      }
      eat(COLON)
      val token: Token = currentToken
      currentToken.kind match {
        case BOOLEAN  =>
          tpe = BooleanType().setPos(currentToken.asInstanceOf[Positioned])
          eat(BOOLEAN)
        case INT  =>
          tpe = IntType().setPos(currentToken.asInstanceOf[Positioned])
          eat(INT)
        case STRING =>
          tpe = StringType().setPos(currentToken.asInstanceOf[Positioned])
          eat(STRING)
        case UNIT =>
          tpe = UnitType().setPos(currentToken.asInstanceOf[Positioned])
          eat(UNIT)
        case IDKIND =>
          tpe = Identifier(token.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
      }
      eat(EQSIGN)
      expr = parseExpression()
      eat(SEMICOLON)

      VarDecl(tpe, id, expr).setPos(pos)
    }

    def parseLambdaDecl: LambdaDecl = {
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      var retType: TypeTree = null
      var id: Identifier = null
      var args = new ListBuffer[Formal]
      var retExpr: ExprTree = null
      var repeatFlag: Boolean = false

      eat(DEF)
      currentToken.kind match {
        case IDKIND =>
          id = Identifier(currentToken.asInstanceOf[ID].value).setPos(pos)
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in VarDecl", pos)
      }
      eat(COLON)
      val token: Token = currentToken
      currentToken.kind match {
        case BOOLEAN  =>
          retType = BooleanType().setPos(currentToken.asInstanceOf[Positioned])
          eat(BOOLEAN)
        case INT  =>
          retType = IntType().setPos(currentToken.asInstanceOf[Positioned])
          eat(INT)
        case STRING =>
          retType = StringType().setPos(currentToken.asInstanceOf[Positioned])
          eat(STRING)
        case UNIT =>
          retType = UnitType().setPos(currentToken.asInstanceOf[Positioned])
          eat(UNIT)
        case IDKIND =>
          retType = Identifier(token.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
      }
      eat(EQSIGN)
      eat(LSQUARE)
      while(currentToken.kind!=RSQUARE) {
        if(repeatFlag)  eat(COMMA)
        repeatFlag = true
        var argID: Identifier = null
        currentToken.kind match {
          case IDKIND =>
            argID = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
            eat(IDKIND)
          case _  =>
            Reporter.error("Error in parseLambdaDecl", pos)
        }
        eat(COLON)
        args += Formal(parseType(), argID)
      }
      eat(RSQUARE)
      eat(LAMBDA)
      retExpr = parseExpression()
      eat(SEMICOLON)

      LambdaDecl(retType, id, args.toList, retExpr)
    }

    def parseMainDecl(): MainDecl = {
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      var obj: Identifier = null
      var parent: Identifier = null
      var varList = new ListBuffer[VarDecl]
      var lambdaList = new ListBuffer[LambdaDecl]
      var exprList = new ListBuffer[ExprTree]

      eat(OBJECT)
      currentToken.kind match {
        case IDKIND =>
          obj = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in VarDecl", pos)
          sys.exit(1)
      }
      eat(EXTENDS)
      currentToken.kind match {
        case IDKIND =>
          parent = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in VarDecl", pos)
      }
      eat(LBRACE)
      while(currentToken.kind == VAR) varList += parseVarDecl
      while(currentToken.kind == DEF) lambdaList += parseLambdaDecl
      exprList += parseExpression
      while(currentToken.kind == SEMICOLON) {
        eat(SEMICOLON)
        exprList += parseExpression
      }
      eat(RBRACE)

      MainDecl(obj, parent, varList.toList, lambdaList.toList, exprList.toList).setPos(pos)
    }

    def parseClassDecl(): ClassDecl = {
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      var id: Identifier = null
      var parent: Option[Identifier] = None
      var varList = new ListBuffer[VarDecl]
      var methodsList = new ListBuffer[MethodDecl]

      eat(CLASS)
      currentToken.kind match {
        case IDKIND =>
          id = Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned])
          eat(IDKIND)
        case _  =>
          Reporter.error("Error in VarDecl", pos)
      }
      if(currentToken.kind == EXTENDS) {
        eat(EXTENDS)
        currentToken.kind match {
          case IDKIND =>
            parent = Some(Identifier(currentToken.asInstanceOf[ID].value).setPos(currentToken.asInstanceOf[Positioned]))
            eat(IDKIND)
          case _  =>
            Reporter.error("Error in VarDecl", pos)
        }
      }
      eat(LBRACE)
      while(currentToken.kind == VAR) varList += parseVarDecl()
      while(currentToken.kind == OVERRIDE || currentToken.kind == DEF)  methodsList += parseMethodDecl()
      eat(RBRACE)

      ClassDecl(id, parent, varList.toList, methodsList.toList).setPos(pos)
    }

    def parseProgam(): Program = {
      val pos: Positioned = currentToken.asInstanceOf[Positioned]
      var classList = new ListBuffer[ClassDecl]
      var main: MainDecl = null

      while(currentToken.kind == CLASS) classList += parseClassDecl()
      main = parseMainDecl()
      eat(EOF)

      Program(main, classList.toList).setPos(pos)
    }

    def parseGoal(): Program = {
      parseProgam()
    }

    readToken()
    val tree = parseGoal()
    terminateIfErrors()
    tree
  }
}
