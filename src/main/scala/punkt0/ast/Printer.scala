package punkt0
package ast

import Trees._

object Printer {
  def apply(t: Tree, printID: Boolean = false): String = {
    var ret: String = " "
    t match {
      case Program(main: MainDecl, classes: List[ClassDecl]) =>
        classes.foreach(ret += apply(_, printID))
        ret+= "\n"
        ret += apply(main, printID)
      case MainDecl(obj: Identifier, parent: Identifier, vars: List[VarDecl], lambdas: List[LambdaDecl], exprs: List[ExprTree]) =>
        ret += "object "
        ret += apply(obj, printID)
        ret += " extends "
        ret += apply(parent, false)
        ret += " {"
        ret+= "\n"
        vars.foreach(ret += apply(_, printID))
        ret += apply(exprs.head, printID)
        exprs.tail.foreach(ret += ";\n" + apply(_, printID))
        ret+= "\n"
        ret += "}"
      case ClassDecl(id: Identifier, parent: Option[Identifier], vars: List[VarDecl], methods: List[MethodDecl]) =>
        ret += "class "
        ret += apply(id, printID)
        if(parent.nonEmpty) {
          ret += " extends "
          ret += apply(parent.get, printID)
        }
        ret += " {"
        ret+= "\n"
        vars.foreach(ret += apply(_, printID))
        methods.foreach(ret += apply(_, printID))
        ret += "}"
        ret+= "\n"
      case VarDecl(tpe: TypeTree, id: Identifier, expr: ExprTree) =>
        ret += "var "
        ret += apply(id, printID) + ": "
        ret += apply(tpe, printID) + " = "
        ret += apply(expr, printID) + ";"
        ret+= "\n"
      case MethodDecl(overrides: Boolean, retType: TypeTree, id: Identifier, args: List[Formal], lambdaArgs: List[LambdaInit], vars: List[VarDecl], lambdas: List[LambdaDecl], exprs: List[ExprTree], retExpr: ExprTree) =>
        if(overrides)
          ret += "override "
        ret += "def " + apply(id, printID) + "("
        if(args.nonEmpty) {
          ret += apply(args.head, printID)
          args.tail.foreach(ret += ", " + apply(_, printID))
        }
        ret += "): " + apply(retType, printID) + "= {"
        ret+= "\n"
        vars.foreach(ret += apply(_, printID))
        if(exprs.nonEmpty) {
          ret += apply(exprs.head, printID)
          exprs.tail.foreach(ret += ";\n" + apply(_, printID))
          ret += ";\n"
        }
        ret+= "\n"
        ret += apply(retExpr, printID)
        ret+= "\n"
        ret += "}"
        ret+= "\n"
      case Formal(tpe: TypeTree, id: Identifier) =>
        ret += apply(id, printID) + ": " + apply(tpe, printID)
      case BooleanType() =>
        ret += "Boolean"
      case IntType() =>
        ret += "Int"
      case StringType() =>
        ret += "String"
      case UnitType() =>
        ret += "Unit"
      case And(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "&&" + apply(rhs, printID) + ")"
      case Or(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "||" + apply(rhs, printID) + ")"
      case Plus(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "+" + apply(rhs, printID) + ")"
      case Minus(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "-" + apply(rhs, printID) + ")"
      case Times(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "*" + apply(rhs, printID) + ")"
      case Div(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "/" + apply(rhs, printID) + ")"
      case LessThan(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "<" + apply(rhs, printID) + ")"
      case Equals(lhs: ExprTree, rhs: ExprTree) =>
        ret += "(" + apply(lhs, printID) + "==" + apply(rhs, printID) + ")"
      case MethodCall(obj: ExprTree, meth: Identifier, args: List[ExprTree], lambdas: List[ExprTree]) =>
        ret += apply(obj, printID) + "."
        if(printID) ret += meth.value + "#??" + "("
        else  ret += apply(meth, printID) + "("
        if(args.nonEmpty) {
          ret += apply(args.head, printID)
          args.tail.foreach(ret += "," + apply(_, printID))
        }
        ret += ")"
      case IntLit(value: Int) =>
        ret += " " + value.toString + " "
      case StringLit(value: String) =>
        ret += "\"" + value + "\""
      case True() =>
        ret += " true "
      case False() =>
        ret += " false "
      case id: Identifier =>
        if(printID)
          ret += id.getSymbol
        else
          ret += id.value
      case This() =>
        ret += " this "
      case Null() =>
        ret += " null "
      case New(tpe: Identifier) =>
        ret += " new " + apply(tpe, printID) + "()"
      case Not(expr: ExprTree) =>
        ret += " ! " + apply(expr, printID)
      case Block(exprs: List[ExprTree]) =>
        ret += "{"
        ret+= "\n"
        if(exprs.nonEmpty) {
          ret += apply(exprs.head, printID)
          exprs.tail.foreach(ret += ";\n" + apply(_, printID))
          ret += "\n"
        }
        ret += "}"
        ret+= "\n"
      case If(expr: ExprTree, thn: ExprTree, els: Option[ExprTree]) =>
        ret += "if "
        ret += "(" + apply(expr, printID) + ")"
        ret+= "\n"
        ret += apply(thn, printID)
        ret+= "\n"
        if(els.nonEmpty) {
          ret += "else"
          ret+= "\n"
          ret += apply(els.get, printID)
          ret+= "\n"
        }
      case While(cond: ExprTree, body: ExprTree) =>
        ret += "while"
        ret += "(" + apply(cond, printID) + ")"
        ret+= "\n"
        ret += apply(body, printID)
        ret+= "\n"
      case Println(expr: ExprTree) =>
        ret += "println(" + apply(expr, printID) + ")"
        ret+= "\n"
      case Assign(id: Identifier, expr: ExprTree) =>
        ret += apply(id, printID) + " = " + apply(expr, printID)
        ret+= "\n"
      case LambdaCall(meth: Identifier, args: List[ExprTree]) =>
    }
    ret += " "

    ret
  }
}
