name := "punkt0"

version := "1.0"

scalaVersion := "2.12.3"

scalacOptions ++= Seq("-deprecation", "-unchecked")
javaOptions ++= Seq(
    // -J params will be added as jvm parameters
    "-J-Xmx2G",
    "-J-Xss2M",

    // others will be added as app parameters
    "-Dproperty=true",
    "-port=8080",

    // you can access any build setting/task here
   s"-version=${version.value}"
)
