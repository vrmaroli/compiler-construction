# Lexer

This assignment is the first real part of the course's compiler
project. Your task is to write the lexer for your compiler. Your
compiler should compile programs written in a simple, but nonetheless
interesting, object-oriented programming language. The language is
called "Punkt0" in reference to Scala's core calculus,
[DOT](https://infoscience.epfl.ch/record/215280). The BNF grammar of
Punkt0 follows. In addition, below you are given additional
specifications for the token classes `<IDENTIFIER>`,
`<INTEGER_LITERAL>`, `<STRING_LITERAL>`, and `<EOF>`. Here are some
details you should pay attention to:

* Make sure you recognize keywords as their own token type. The
  keyword `while`, for instance, should be lexed as the token type
  `WHILE`, not as an identifier representing an object called `while`.

* Make sure you correctly register the position of all tokens. Note
  the `Source.pos` and the `Positioned.setPos` methods.

* In general, it is good to output as many errors as possible (this
  helps whoever uses your compiler). For instance, if your lexer
  encounters an invalid character, it can output an error message,
  skip it, and keep on lexing the rest of the input. After lexing, the
  compiler still won't proceed to the next phase, but this helps the
  user correct more than one error per compilation. Use the special
  `BAD` token type to mark errors and keep lexing as long as it is
  possible.

* The Lexer does not immediately read and return all tokens, it
  returns an `Iterator[Token]` that will be used by future phases to
  read tokens on demand.

* Comments should not produce tokens.

* Comments in Punkt0 can be marked using the end-of-line notation
  (`//`) or blocks (`/* .. */`). Nested block comments are **not
  allowed**.

* Your lexer should support a command-line option `--tokens` for
  printing out the tokens that were recognized. With this option, your
  compiler should output one token per line. After all tokens have
  been recognized, your compiler should exit.

* Your compiler should terminate with exit code `1` in case the lexer
  detected errors. See method `Reporter.terminateIfErrors`.

| Non-terminal        |     | Definition                                                                                                                                                                                    |
| ------------------- | --- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| *Program*           | ::= | ( *ClassDeclaration* ) * *MainDeclaration* <EOF>                                                                                                                                              |
| *ClassDeclaration*  | ::= | **class** *Identifier* ( **extends** *Identifier* )? **{** ( *VarDeclaration* ) * ( *MethodDeclaration* ) * **}**                                                                             |
| *MainDeclaration*   | ::= | **object** *Identifier* **extends** *Identifier* **{** ( *VarDeclaration* ) * *Expression* ( **;** *Expression* ) * **}**                                                  |
| *VarDeclaration*    | ::= | **var** *Identifier* **:** *Type* **=** *Expression* **;**                                                                                                                                    |
| *varFormal*         | ::= | *Identifier* **:** *Type*    |
| *lambdaFormal* | ::= | *Identifier* **:** **(** ( *Type* ( **,** *Type* ) * )? **)** **=>** *Type* |
| *LambdaDeclaration* | ::= | **def** *Identifier* **:** *Type* **=** **[** ( *varFormal* ( **,** *varFormal* ) * )? **]** **=>** *Expression* **;**                                                                                                                                    |
| *MethodDeclaration* | ::= | ( **override** )? **def** *Identifier* **(** ( *varFormal* &#124; *lambdaFormal* ( **,** *varFormal* &#124; *lambdaFormal* ) * )? **)** **:** *Type* = **{** ( *VarDeclaration* ) * *Expression* ( **;** *Expression* ) * **}** |
| *Type*              | ::= | **Boolean**                                                                                                                                                                                   |
|                     |     | **Int**                                                                                                                                                                                       |
|                     |     | **String**                                                                                                                                                                                    |
|                     |     | **Unit**                                                                                                                                                                                      |
|                     |     | *Identifier*                                                                                                                                                                                  |
| *Expression*        | ::= | *Expression* ( **&&** &#124; **&#124;&#124;** &#124; **==** &#124; **<** &#124; **+** &#124; **-** &#124; **&#42;** &#124; **/** ) *Expression*                                                                                              |
|                     |     | *Expression* **.** *Identifier* **(** ( *Expression* &#124; ( **^** *LambdaCall* ) ( **,** *Expression* &#124; ( **^** *LambdaCall* ) ) * )? **)**                                                                                                            |
|                     |     | `<INTEGER_LITERAL>`                                                                                                                                                                           |
|                     |     | **"** `<STRING_LITERAL>` **"**                                                                                                                                                                |
|                     |     | **true**                                                                                                                                                                                      |
|                     |     | **false**                                                                                                                                                                                     |
|                     |     | *Identifier*                                                                                                                                                                                  |
|                     |     | **this**                                                                                                                                                                                      |
|                     |     | **null**                                                                                                                                                                                      |
|                     |     | **new** *Identifier* **(** **)**                                                                                                                                                              |
|                     |     | **!** *Expression*                                                                                                                                                                            |
|                     |     | **(** *Expression* **)**                                                                                                                                                                      |
|                     |     | **{** ( *Expression* ( **;** *Expression* ) * )? **}**                                                                                                                                        |
|                     |     | **if** **(** *Expression* **)** *Expression* ( **else** *Expression* )?                                                                                                                       |
|                     |     | **while** **(** *Expression* **)** *Expression*                                                                                                                                               |
|                     |     | **println** **(** *Expression* **)**                                                                                                                                                          |
|                     |     | *Identifier* **=** *Expression*                                                                                                                                                               |
  |                    |    | *LambdaCall* .  |
| *Identifier*        | ::= | `<IDENTIFIER>`                                                                                                                                                                                |
| *LambdaCall*        | ::= | *Identifier* **(** ( *Expression* ( **,** *Expression* ) * )? **)**                                                                            |


Special token classes:

* `<IDENTIFIER>` represents a sequence of letters, digits and
  underscores, starting with a letter and which is not a
  keyword. Identifiers are case-sensitive.

* `<INTEGER_LITERAL>` represents a sequence of digits, with no leading
  zeros.

* `<STRING_LITERAL>` represents a sequence of arbitrary characters,
  except new lines and **"**. We don't ask you to support escape
  characters such as **\n**.

* `<EOF>` represents the special end-of-file character.

# Parser

In this lab, you'll work on the second part of the punkt0 compiler
project. Your goal is to manually implement a recursive-descent parser
to transform programs described by the punkt0 grammar
into abstract syntax trees. You also need to write a pretty-printer
for these trees. This assignment is rather long and we can only
recommend that you start early, that you make sure you understand
every step, and that you ask otherwise.

One way to partially check your implementation is to use the
pretty-printer to output the parsed AST. If you then re-parse this
output and re-pretty-print it, you should obtain exactly the same
result. In other words: for all input programs `P`, you should have
that the identity `print(parse(P)) = print(parse(print(parse(P))))`
holds. While this condition is necessary to ensure you have properly
written your parser and printer, it is not sufficient, and you are as
usual responsible for checking the details of your implementation
before you submit it.

* Write a recursive-descent Parser for punkt0 by manually
  writing mutually recursive procedures, as sketched in the lecture on
  recursive descent parsing, and generating the appropriate abstract
  syntax trees.

* Implement a new command-line option `--ast` for printing out the
  AST.

### Notes

  * It's not allowed in punkt0 to have a variable with a name that
    matches a keyword.

  * You need to properly encode the operator precedence as in Java and
    Scala. From highest priority to lowest: **!**, then **'*'** and
    **/**, then **+** and **-**, then **<** and **==**, then **&&**,
    then **||**.  Except for the unary operators, all operators are
    left-associative.  There is no precedence between operators of the
    same level. For instance: `4 / 3 * 6` reads as `((4 / 3) * 6)` and
    `6 * 4 / 3` as `((6 * 4) / 3`.

  * An `else` keyword always applies to the closest `if`. You can add
    comments to mark the end of `if` blocks in your pretty-printer to
    make sure you parse things properly.
<code java>
if (expr1) if (expr2) a = 1 else a = 2
</code>
...could be reprinted as:
<code java>
if (expr1) { if (expr2) { a = 1 } else { a = 2 } }
</code>
...and should **not** produce:
<code java>
if (expr1) { if (expr2) { a = 1 } } else { a = 2 }
</code>

  * You have to write the parser manually, as a series of mutually
    recursive functions. You are not allowed to use parser combinators
    or other such libraries.

  * You need to set the position of the trees. Normally, the position
    of the tree corresponding to `42 + 23` is the position of the `4`.

  * We provide a number of both valid and invalid test programs which
    you can use to test both your lexer and your parser (there are
    subdirectories "lab2" and "lab3").

  * The provided test programs also include the reference output for
    each test program (tokens for each valid lexer test, AST for each
    valid parser test).

## Pretty-printer

Write a pretty-printer for the AST hierarchy. Your pretty-printer
should transform any valid AST into the corresponding textual
representation in the punkt0 programming language. The output does not
need to be exactly as the input file; whitespaces can be placed
differently, comments will disappear and parentheses can be added (or
removed), as long as the program keeps the original intended
meaning. Re-parsing the pretty-printed output and re-printing it
should yield an identical result, though.

# Name Analysis

In this lab you will add name analysis to your Punkt0 compiler. This
will considerably ease the task of type checking that you will start
afterwards. Note that the type checker assignment will be released
soon, so make sure you start working on the name analysis as early as
possible.

The description of this assignment is rather long. Don't panic! :)
Most of it is to help you avoid forgetting important points, and we
give you a substantial amount of code.

## Symbols

The goal of name analysis is twofold: we want to reject programs which
contain certain types of errors, and we want to associate *symbols* to
all identifiers.

Symbols are values which uniquely identify all class, method and
variable names in a program, by mapping their (multiple) *occurrences*
to their (unique) *definition*. Identifiers are already present in the
AST and contain names as well, but these are not sufficient to
distinguish between a class member and a local variable with the same
name, for instance. In this lab we will--among other things--add this
missing information to the AST.

In the process of mapping occurrences to definitions, we will be able
to detect the following kinds or errors:

* a class is defined more than once
* a variable is defined more than once
* a class member is overloaded (forbidden in Punkt0)
* a method is overloaded (forbidden in Punkt0)
* a method argument is shadowed by a local variable declaration
(forbidden in Java, and we follow this convention)
* two method arguments have the same name
* a class name is used as a symbol (as parent class or type, for
instance), but is not declared
* an identifier is used as a variable, but is not declared
* the inheritance graph has a cycle (e.g., `class A extends B {} class
B extends A {}`)
* (Note that name analysis does not check that method calls
correspond to methods defined in the proper class. We will need
type checking for this.)


## Implementation

In order to attach symbols to trees, we define a new trait,
`Symbolic`, and a new set of classes for the symbols. The `Symbolic`
trait is parametrized by a class name which allows us to define the
kind of symbols which can be attached to each kind of AST node (see
`Symbols.scala` and `Trees.scala` later for examples).

You need to write your analyzer such that two nodes referencing the
same symbol have the same symbol class __instance__ attached to them
(that is, reference equality, structural equality is not enough). We
defined the `Symbol` class such that symbols automatically get a
unique identifier attached to them at creation. This will allow you to
check that you are attaching symbols correctly: you will add an option
to your pretty-printer to be able to print these unique numbers along
with the identifiers where they occur.

Note that `Symbol`s are also `Positional` objects, which means you
have to set them a correct position. The position of the symbol should
be its declaration position. This is necessary to produce meaningful
error messages such as `"error: class Cl is defined twice. First
definition here: ..."`.

### Internal errors

When your compiler encounters an *internal* error (for example, a
scope is not initialized as you expected, a symbol is `null`, etc.),
you should **not** use the methods from the reporter trait. You must
use `sys.error` instead, which will throw an exception and show you
the stack trace. The reason is that you shouldn't blame the user for
internal errors. In fact, the user should never encounter an internal
error. Of course, writing bug-free programs is hard...


### Symbols as scopes

We will take advantage of the fact that scopes in Punkt0 are only of
three kinds:

- the global scope (the set of declared classes, including a
synthetic class for the main method)
- the class scopes (all members and methods within a class, plus the
global scope)
- the method scopes (the parameters and local variables, plus the
corresponding class scope)

This in fact defines a hierarchy among symbols:

* all class symbols are defined in the global scope
* all methods are defined in a class scope
* variables are defined either in a method scope, as parameters or
locals, or in a class scope

We encoded this hierarchy in the symbol classes. Therefore, if we have
access to a class symbol, for instance, and all symbols were correctly
set, we can access from there all method symbols and member variable
symbols. This will allow us to easily check if a variable was
declared, for instance.

### Two phases

Here is how we recommend you proceed for the implementation:

- First, collect all symbols: create the symbol class instances, and
attach them to field types, method member types, formal parameter
types and method return types.

- Make the appropriate changes to your pretty-printer and make sure
you see the unique IDs next to the identifiers at the definition
points.

- Implement the second phase of your analyzer which consists in
attaching the proper symbol to the occurrences of the
identifiers. To simplify your task, start by writing `lookup*`
methods in the symbol classes: they will allow you to easily check
whether an identifier was declared and to recover its symbol if it
was. Make sure you properly encode the scope rules (including
shadowing) in your `lookup*` methods.

- You can use your pretty-printer to make sure you attached symbols
correctly.

- Make sure that you throw errors and warnings when appropriate.


### Execution example

When analyzing the following file:

class B extends A {
override def foo(): Int = {
value = 42;
value
}
}

class A {
var value: Int = 0;
def foo(): Int = {
var value: Boolean = true;
value = false;
41
}
}

object Main extends App {
println(new B().foo())
}

The pretty-printer would output something like (if the `--symid`
command-line option is provided):

class B#2 extends A#3 {
override def foo#7(): Int = {
value#4 = 42;
value#4
}
}

class A#3 {
var value#4: Int = 0;
def foo#5(): Int = {
var value#6: Boolean = true;
value#6 = false;
41
}
}

object Main#1 extends App {
println((new B#2().foo#??()))
}

Note that:

* Overriding methods have a different symbol than their overridden
counterparts.

* Method names in method calls are unresolved symbols.

### Constraints

Here are all the constraints that your analyzer should enforce (note
that this is simply a reformulation of the types of errors we want to
catch):

#### Variable declarations

* No two variables can have the same name in the same scope, unless
one of the two cases of shadowing occurs.
* All used variables must be declared.
* The initializer expression in a variable or field declaration must
be either a constant (including `null`) or a `new` expression
(instance creation). *Note:* you can implement this constraint by
modifying your parser to incorporate this restriction. (An
alternative is to enforce this constraint directly in the name
analyzer.)

#### Shadowing

Shadowing can occur in two different situations:

- a local variable in a method can shadow a class member
- a method parameter can shadow a class member

All other types of shadowing are not allowed in Punkt0.

#### Classes

* Classes must be defined only once.
* When a class is declared as extending another one, the other class
must be declared.
* The transitive closure of the `extends` relation must be irreflexive
(no cycles in the inheritance graph).
* When a class name is used as a type, the class must be declared.

#### Overloading

* Overloading is not permitted:
* In a given class, no two methods can have the same name.
* In a given class, no method can have the same name as another
method defined in a super class, unless overriding applies.

#### Overriding

* A method in a given class overrides another one in a super class
if they have the same name and the same number of arguments. (Of
course this constraint will be tightened once we start checking
types.) An overriding method must have an `override` modifier.
* Fields cannot be overridden.

# Type checking

A valid Punkt0 program has the following properties:

* It follows the Punkt0 concrete syntax.
* It respects all the constraints mentioned in [Lab 4](lab4.html).
* Method overriding respects some typing constraints:
  * The overriding method must have exactly as many parameters as the overridden one.
  * The types of the parameters in the overriding and overridden methods must match exactly (no contravariance allowed).
  * The return type must match exactly (no covariance allowed).
  * The overriding method must carry the `override` modifier.
* All expressions typecheck and have the expected type (the returned expression matches the declared return type, for instance).

Your goal in this assignment is to enforce all the constraints not
enforced already by the previous phases.

**Note:** The language and the type rules presented in the course may
differ from the rules of Punkt0. If there are any differences, please
use the description on the current page for your implementation, and
not the rules in the lecture. Of course, feel free to clarify with
us if you have any questions.

## Types

The following primitive types exist in Punkt0 (note that we prefix them
with **T** to differentiate them from the tree nodes with the same
name, for instance):

  * **TBoolean**
  * **TInt**
  * **TString** (We consider **String** to be a primitive type, unlike in Java where it is a proper class. No methods can be called on **String**s, for instance.)
  * **TUnit**

Additionnally, we have class types:

  * **TClass[*name*]**

We define a subtyping relation on these types. All primitive types are
subtypes of themselves and of no other type. For instance:

  * **TInt <: TInt**

All class types are subtypes of themselves and the special **AnyRef**
class type. The subtyping relation is also transitive.

  * **TClass[*name*] <: TClass[*name*]** and **TClass[*name*] <: TClass[**AnyRef**]**
  * **TClass[**B**] <: TClass[**A**]** and **TClass[**C**] <: TClass[**B**]** implies **TClass[**C**] <: TClass[**A**]**

With this in mind, we give some of the non-trivial typing
constraints. This is naturally not an exhaustive list of what you
should implement, but we expect you to be able to deduce the other
rules unambiguously yourself (if in doubt about a rule, ask on KTH
Social).

### Overloaded `+`

The `+` operator can represent integer addition, or string
concatenation. If the types of **e**1 and **e**2 are **T**1 and **T**2
respectively, we have for the type **T**s of **e**1 + **e**2:

  * **T**1 = **TInt** and **T**2 = **TInt** implies **T**s = **TInt**
  * **T**1 = **TString** and **T**2 = **TInt** implies **T**s = **TString**
  * **T**1 = **TInt** and **T**2 = **TString** implies **T**s = **TString**
  * **T**1 = **TString** and **T**2 = **TString** implies **T**s = **TString**

All other values for **T**1 and **T**2 should result in type errors.

### Comparison operator

The `==` operator is also overloaded. Expression `e1 == e2` is type
correct if and only if one of the following two cases applies:

  * `e1` and `e2` have both primitive types, and these types are equal
  * `e1` and `e2` have both class types (in which case they can be different classes)

Note that it is **not** type correct to compare a primitive type to a
class type. Again, strings are considered **primitive**.

Consider the following code.

    class A {}
    class B {}

Let `e1: T1` and `e2: T2`. Then the following table summarizes some of the cases for `e1 == e2`.

T1     | T2     | type checks?
--     | --     | ------------
Int    | Int    | yes
String | String | yes
String | A      | no
A      | Int    | no
A      | B      | yes

### Method calls

The dereferenced object must be of a class type, and its class must
declare or inherit the called method. The number of arguments must of course
match the number of parameters. The passed arguments must have
subtypes of the declared parameters (matching one-by-one).

### Assignment

Assignment of an expression of type **T** can only be done to a
variable of type **S** such that **T <: S**.

The type of an assignment expression is **Unit**.

It is not allowed to reassign method parameters.

### This

`this` is always considered to carry the class type corresponding to
the class where it occurs.


### Returned expression

The returned expression must be of a subtype of the declared return
type.


### The `println` expression

We will consider `println` calls to be type correct if the argument has type **String**, **Int**, or **Boolean**. The type of a `println` expression is **Unit**.


### The `while` expression

The type of a **while** expression is **Unit**. Its conditional
expression must have type **Boolean**, and its body must have type
**Unit**.


### The `if` expression

The type of an `if` expression is the least upper bound of the types of the two branches. Its conditional expression must have type **Boolean**.

### The block expression

The type of a block expression is the type of the block's last expression.

### The main declaration

The main `object` declaration must extend the built-in `App`
type. (This is important to ensure Punkt0 is a subset of Scala.)


### Variable declarations

The constant initial expression must be of the correct, declared type.

## Suggested implementation

Here are the steps we suggest you take:

* Modify your analyzer such that it attaches types to the various
  symbols. Since symbols are shared, this has the advantage that you
  can recover the proper type from any occurrence of the symbol.

* Modify your analyzer so that it enforces the overriding type
  constraints on methods.

* Implement your typechecker. Make sure you attach the types to the
  expression subtrees (this will be required for code generation, to
  differentiate between the versions of the overloaded operators, for
  instance).

* While you typecheck expressions, attach the proper symbols to the
  occurrences of method calls (since you can now determine the class
  from which they are called; though, what you are able to determine
  may not match the run-time types).

* Test, test, test!

## User-friendliness

It is very important that your compiler does not stop at the first
type error! `TypeChecker.scala` contains some hints on how to achieve
this. Detect as many errors as possible!

# Code Generation

Generating class files is a tedious and not so interesting task. Therefore you will use a library for this. Please read the [Cafebabe documentation pages](https://github.com/psuter/cafebabe/wiki).

**Please note that the project uses an older version of scala!** The provided jar has the version bumped to scala 2.12. The source can be found here: [phaller/cafebabe](https://github.com/phaller/cafebabe). The only file that differs is *build.sbt*.


## Printing strings, booleans, and integers

To print strings you will need to call a Java library method (there are no bytecodes to do that directly). For this, you need to invoke `System.out.println(...)` of Java's standard library. `System.out` is a static field, so you first need to emit a `GETSTATIC` bytecode, then emit the code for the expression you're trying to print, then emit an `INVOKEVIRTUAL` bytecode. `System.out` is of type `java.io.PrintStream`. How to generate this particular sequence of instructions is shown in [this example](https://github.com/psuter/cafebabe/wiki/FullExamples#hello-world) on the Cafebabe wiki. If in doubt, compile an example with "javac" and run "javap -c" on the ".class" file to see what it did.

Converting a boolean or an integer to a string is done using the `java.lang.StringBuilder` class. The procedure consists in creating a new instance of `StringBuilder`, then appending to it the value you want to convert, then calling `toString()` on the builder instance. The `append` method is overloaded for booleans and integers (calling types `(Z)Ljava/lang/StringBuilder;` and `(I)Ljava/lang/StringBuilder;`), and you're not asked to be able to convert any other type (... but you are of course free to handle the conversion of arbitrary objects using `toString()`).

## Concatenating strings

Concatenating strings is done using the `java.lang.StringBuilder` class. The procedure consists in creating a new instance of `StringBuilder`, then appending to it whatever you want to concatenate together, then calling `toString()` on the builder. The `append` method is overloaded for strings and integers, and you're not asked to be able to concatenate any other type.

## Equality

We will handle equality in a simple way:

  * integers and booleans are compared by value
  * other types (strings, arrays, and objects) are compared by reference. In the case of strings, the result may or may not be the same as calling `.equals(...)`, depending on whether the strings are constant. (In other words, don't do anything special for equality.)

## Boolean expressions

You have to apply **lazy evaluation** to boolean expressions (short-circuit). Make sure your compiled code for expressions such as `(true || (1/0 == 1))` doesn't crash.

## Notes on types in the JVM

The various naming conventions in the JVM can be confusing: some bytecodes contain a letter indicating to which type of operands they apply (e.g., `ILOAD` which loads integers, `IF_ACMPEQ` which compares two references for equality, or `LRETURN` which returns a "long" value). In this convention, we have:

|  Letter  |  Corresponding type  |
|----------|----------------------|
|  I  |  Integer   |
|  L  |  Long      |
|  D  |  Double    |
|  F  |  Float     |
|  A  |  Reference (object or array)  |

...but then there is *another convention* when we want to describe field or method types and method signatures (see [this section](https://github.com/psuter/cafebabe/wiki#types-in-class-files)). In particular, note that `L` is used for objects (but not arrays) in that second convention, but for ''long'' operands in bytecodes.

Finally, note that returning from a method with return type `Unit` (a ''void'' method) is done using the `RETURN` bytecode (no prefix letter).

## Task

Complete the stub for `CodeGeneration.scala` such that your compiler emits class files. You should be able to run the main method with `java Main` and get the same result as with "scalac" for valid Punkt0 programs.

Your compiler should generate class files (one per class, as in Java, and one for the main object) silently if the compilation is successful, or generate errors and warnings from the previous phases in case there was a problem. The code generation phase should not produce any error, and the generated class files should be executable on the Java Virtual Machine. You should store all the relevant meta information in the class files: line numbers and source file identification (see the Cafebabe documentation).

## References

  * [The complete JVM specification](http://docs.oracle.com/javase/specs/jvms/se8/html/index.html) (may be hard to digest)
  * [JVM opcodes arranged conveniently](http://homepages.inf.ed.ac.uk/kwxm/JVM/index.html), in particular: [by function](http://homepages.inf.ed.ac.uk/kwxm/JVM/codeByFn.html)
  * [Cafebabe documentation](https://github.com/psuter/cafebabe/wiki)

# Compiler Extension - First-class Functions: Functions as values

Allow functions to be passed as arguments. Functions passed can be predefined regular functions of a specified signature or even anonymous functions defined during method call. These features enable programmers to write clearer programs because the intended meaning of the program is more implicit rather than having to be explicitly stated with comments. The changes only make the language more programmer friendly, the passed functions get executed just like other normal functions and so does not improve efficiency.
In short, it is much easier to understand the program and the programmer�s intentions are more clearly expressed in the code.

## Implementation
### Lexer
No changes required.
### Parser
* Adding MethodSignature to AST, which is very similar to Formal but represents a function signature (type of each argument (including the number and order) and return type).
* Adding List[MethodSignature] to MethodDecl.
* Change to grammar of MethodDecl: To make things easier, we can have all the regular variables first and then the methods to be passed if any.
* Allowing anonymous functions: we could maybe just generate a unique name in this case. But grammar changes required to allow function definition inside method calls.
### Name analysis
* MethodSymbol: Some changes to store the names of methods received as arguments and assigning unique IDs which could be used in code generation phase.
### Type checking
* MethodCall: Check if the passed function�s signature matches that specified in MethodDecl.
* Verify function signature again when the passed function is being called.
### Code generation
* Store all functions in a hidden class and invoke them when necessary.
* Maybe use a map and some kind of ID numbers to figure out which functions need to be called when.

## Further extensions

* Allow functions to be returned.
* Allow passing simpler lambda expression as argument.


